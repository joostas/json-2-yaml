using System;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Input;

/// <summary>
/// This is added to be able to do integration tests for reading from redirected stdin.
/// Issue is that flag Console.IsInputRedirected isn't updated when setting Console.SetIn(). 
/// and we need this command in integration tests to immitate stdin redirection.
/// see https://github.com/dotnet/corefx/issues/10428
/// </summary>
public class ConsoleRedirectionMarker : IConsoleRedirectionMarker
{
    /// <summary>
    /// Returns default Console.IsInputRedirected flag.
    /// </summary>
    public bool IsInputRedirected => Console.IsInputRedirected;

    /// <summary>
    /// In real code we are able to update encoding, but in integration tests - not. Why, don't know.
    /// </summary>
    public bool CanUpdateEncoding => true;
}