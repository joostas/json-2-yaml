using System;
using Autofac;
using Json2Yaml.Input;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Input
{
    /// <summary>
    /// Creates input reader.
    /// </summary>
    public class InputReaderFactory : IInputReaderFactory
    {
        private readonly ILifetimeScope _scope;
        public InputReaderFactory(ILifetimeScope scope)
        {
            _scope = scope;
        }

        /// <summary>
        /// Creates input reader based on provided enum value.
        /// </summary>
        /// <returns>Input reader instance</returns>
        public IInputReader Create(InputType inputType, bool skipContentTypeCheck)
        {
            switch (inputType)
            {
                case InputType.File:
                    return _scope.Resolve<FileInputReader>();
                case InputType.Http:
                    return _scope.Resolve<HttpInputReader>(new TypedParameter(typeof(bool), skipContentTypeCheck));
                case InputType.Console:
                    return _scope.Resolve<ConsoleInputReader>();
                default:
                    throw new ArgumentException($"{nameof(inputType)}: value {inputType} is not supported");
            }
        }
    }
}