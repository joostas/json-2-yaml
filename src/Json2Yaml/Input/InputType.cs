namespace Json2Yaml.Input
{
    /// <summary>
    /// Defines input values
    /// </summary>
    public enum InputType
    {
        None = 0,
        File = 1,
        Http = 2,
        Console = 3,
    }
}