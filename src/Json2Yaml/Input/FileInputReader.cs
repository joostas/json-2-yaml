using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Input
{
    public class FileInputReader : IInputReader
    {
        private readonly IIOHandler _ioHandler;
        public FileInputReader(IIOHandler iohandler)
        {
            _ioHandler = iohandler;
        }

        public Task<string> ReadJsonAsync(string path, CancellationToken cancellationToken) =>
            _ioHandler.ReadFileAsync(path, cancellationToken);

        public Task<Stream> OpenStreamAsync(string path, CancellationToken _) =>
            Task.FromResult<Stream>(File.OpenRead(path));
    }
}