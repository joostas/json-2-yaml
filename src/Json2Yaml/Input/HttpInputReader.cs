using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Input
{
    public class HttpInputReader : IInputReader
    {
        private readonly IRemoteIOHandler _remoteIOHandler;
        private readonly bool _skipContentTypeCheck;

        public HttpInputReader(IRemoteIOHandler remoteIOHandler, bool skipContentTypeCheck)
        {
            _remoteIOHandler = remoteIOHandler;
            _skipContentTypeCheck = skipContentTypeCheck;
        }

        public Task<Stream> OpenStreamAsync(string path, CancellationToken token)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException($"'{nameof(path)}' cannot be null or whitespace.", nameof(path));
            }

            return _remoteIOHandler.FetchStreamAsync(path, _skipContentTypeCheck, token);
        }

        public async Task<string> ReadJsonAsync(string path, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException(nameof(path));
            }
            if (!path.StartsWith("http://") && !path.StartsWith("https://"))
            {
                path = "http://" + path;
            }
            return await _remoteIOHandler.FetchJsonAsync(path, _skipContentTypeCheck, cancellationToken);
        }
    }
}