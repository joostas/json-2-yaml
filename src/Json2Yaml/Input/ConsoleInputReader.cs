using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Input;

/// <summary>
/// Reads input from stdin.
/// </summary>
public class ConsoleInputReader : IInputReader
{
    private readonly IConsoleRedirectionMarker _marker;

    public ConsoleInputReader(IConsoleRedirectionMarker marker)
    {
        _marker = marker ?? throw new ArgumentNullException(nameof(marker));
    }

    /// <summary>
    /// Reads input from stdin.
    /// </summary>
    /// <param name="_">Not actual. Will be ignored.</param>
    /// <param name="__">Not actual. Will be ignored.</param>
    /// <returns></returns>
    public Task<string> ReadJsonAsync(string _, CancellationToken __)
    {
        if (!_marker.IsInputRedirected)
        {
            throw new Json2YamlException("No input from stdin");
        }

        Encoding currentInputEncoding = SetInputEncoding(Encoding.UTF8);

        var stringBuilder = new StringBuilder();
        string line;
        do
        {
            line = Console.ReadLine();
            stringBuilder.AppendLine(line);
        } while (line != null);

        SetInputEncoding(currentInputEncoding);
        return Task.FromResult(stringBuilder.ToString());

        Encoding SetInputEncoding(Encoding encoding)
        {
            if (!CanUpdateEncoding() || encoding == null)
            {
                return null;
            }
            if (Console.InputEncoding != encoding)
            {
                Console.InputEncoding = encoding;
            }
            return encoding;
        }

        /// found that encoding cannot be updated if using Console.SetIn 
        /// (SetIn is used in integration testing). This method checks if this is not that case.
        bool CanUpdateEncoding() =>
            _marker.CanUpdateEncoding;
    }

    public async Task<Stream> OpenStreamAsync(string _, CancellationToken __)
    {
        var input = await ReadJsonAsync(_, __);
        return new MemoryStream(Encoding.UTF8.GetBytes(input ?? ""));
    }
}