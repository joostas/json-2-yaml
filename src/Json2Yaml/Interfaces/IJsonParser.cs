using System.IO;

namespace Json2Yaml.Interfaces
{
    public interface IJsonParser
    {
        object ParseJson(string json);

        object ParseJson(Stream jsonStream);
    }
}