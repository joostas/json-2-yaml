using System.IO;

namespace Json2Yaml.Interfaces
{
    public interface IYamlProducer
    {
        string ProduceYaml(object json, IYamlFormatSettings yamlFormatSettings);

        Stream ProduceYamlStream(object json, IYamlFormatSettings yamlFormatSettings);
    }
}