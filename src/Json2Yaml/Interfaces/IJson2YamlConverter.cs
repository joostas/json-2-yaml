using System.IO;
using Json2Yaml.Json;

namespace Json2Yaml.Interfaces;

public interface IJson2YamlConverter
{
     string Convert(string jsonInput, JsonParserSettings jsonParserSettings, IYamlFormatSettings formatSettings);

     // string Convert(Stream jsonInput, JsonParserSettings jsonParserSettings, IYamlFormatSettings formatSettings);

     Stream Convert(Stream jsonInput, JsonParserSettings jsonParserSettings, IYamlFormatSettings formatSettings);
}
