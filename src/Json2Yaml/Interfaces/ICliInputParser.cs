using System.Collections.Generic;
using Json2Yaml.Cli;

namespace Json2Yaml.Interfaces
{
    /// <summary>
    /// Adapter to work with command input arguments parsed by CommandLineUtils lib.
    /// </summary>
    public interface ICliInputParser
    {
        /// <summary>
        /// Reads command arguments input from structure provided by CommandLineUtils tool after argument parsing.
        /// </summary>
        /// <param name="parseResults">CommandLineUtils framework structure created after parsing command line arguments.</param>
        /// <returns>Own structure of parsed arguments. See <see cref="CommandOptions"/>.</returns>
        /// <exception cref="ArgumentNullException">Argument null exception.</exception>
         CommandOptions ParseArguments(ParsedCommand parseResults);


         /// <summary>
         /// Returns if info can be writen to console.
         /// </summary>
         /// <param name="parseResults">CommandLineUtils framework structure created after parsing command line arguments.</param>
         /// <remarks>This answer should be known before full parsing, therefore this method is implemented separately.</remarks>
         bool CanWriteInfoToConsole(List<ParsedOption> options);

         /// <summary>
         /// Returns if info should be writen in verbose mode.
         /// </summary>
         /// <param name="parseResults">CommandLineUtils framework structure created after parsing command line arguments.</param>
         /// <remarks>This answer should be known before full parsing, therefore this method is implemented separately.</remarks>
         bool IsVerbose(List<ParsedOption> options);
    }
}