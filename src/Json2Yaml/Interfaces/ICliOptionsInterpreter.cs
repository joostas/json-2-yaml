using Json2Yaml.Utils;

namespace Json2Yaml.Interfaces
{
    /// <summary>
    /// Evaluates parsed cli arguments, prepares input for json2yaml converter.
    /// </summary>
    public interface ICliOptionsInterpreter
    {
         (bool passed, string errorMessage) AreCorrectInputOptionsProvided();

         ConversionContext PrepareForConversion();
    }
}