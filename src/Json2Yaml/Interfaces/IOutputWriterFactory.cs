namespace Json2Yaml.Interfaces
{
    public interface IOutputWriterFactory
    {
        IOutputWriter Create(bool writeToConsole, bool verbose);
    }
}