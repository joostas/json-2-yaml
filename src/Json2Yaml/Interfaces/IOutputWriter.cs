using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Json2Yaml.Interfaces
{
    public interface IOutputWriter
    {
         /// <summary>
         /// Write supporting output. Like how log tool run or similar.
         /// </summary>
         void WriteInfo(string message);

         /// <summary>
         /// Write tool conversion output.
         /// </summary>
         Task WriteOutputAsync(string output, string path, CancellationToken token);

        /// <summary>
        /// Write tool conversion output.
        /// </summary>
        Task WriteOutputAsync(Stream output, string path, CancellationToken token);

         /// <summary>
         /// Write error what went wrong. Can be redirected to stderr.
         /// </summary>
         void WriteError(Exception ex);

         /// <summary>
         /// Write error what went wrong. Can be redirected to stderr.
         /// </summary>
         void WriteError(string message);
    }
}