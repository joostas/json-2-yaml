namespace Json2Yaml.Interfaces
{
    public interface IUrlHelper
    {
         bool CheckIfUrlCorrect(string url);

         /// <summary>
         /// Constructs file name based on last segment of given url.
         /// </summary>
         /// <param name="url"></param>
         /// <returns>If segment contains extension it is replaced with .yaml.
         /// If segment doesn't contain extension .yaml is added.</returns>
         string ExtractFileName(string url);
    }
}