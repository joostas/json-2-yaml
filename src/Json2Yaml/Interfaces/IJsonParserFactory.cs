using Json2Yaml.Json;

namespace Json2Yaml.Interfaces;

public interface IJsonParserFactory
{
    IJsonParser Create(JsonParserSettings settings);
}
