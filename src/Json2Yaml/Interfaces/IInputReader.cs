using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Json2Yaml.Interfaces
{
    public interface IInputReader
    {
        Task<string> ReadJsonAsync(string path, CancellationToken token);

        Task<Stream> OpenStreamAsync(string path, CancellationToken token);
    }
}