using System;
using System.Collections.Generic;
using Json2Yaml.Cli;

namespace Json2Yaml.Interfaces
{
    public interface ICliApplicationHelper
    {
        (bool isDefined, string value) GetValueInfo(List<ParsedOption> options, string optionName);
        OptionInfo<T> GetValueInfo<T>(List<ParsedOption> options, string optionName);
        OptionInfo<T> GetValueInfo<T>(List<ParsedOption> options, string optionName, Func<string, T> converter);
    }
}