using System;
using System.Threading;
using System.Threading.Tasks;

namespace Json2Yaml.Interfaces
{
    public interface IJson2YamlCli
    {

         Task<int> RunAsync(CancellationToken cancellationToken = default);

         void Initialize();

         void ParseArguments(string[] args);

         bool CanWriteInfoToConsole {get;}

         IOutputWriter Output {get;}

         event EventHandler OutputInitialized;
    }
}