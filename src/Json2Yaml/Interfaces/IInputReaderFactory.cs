using Json2Yaml.Input;

namespace Json2Yaml.Interfaces
{
    public interface IInputReaderFactory
    {
        IInputReader Create(InputType inputType, bool skipContentTypeCheck);
    }
}