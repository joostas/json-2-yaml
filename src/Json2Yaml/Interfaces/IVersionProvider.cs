namespace Json2Yaml.Interfaces
{
    public interface IVersionProvider
    {
         string ReadProjectVersion();
    }
}