using Json2Yaml.Yaml;

namespace Json2Yaml.Interfaces
{
    /// <summary>
    /// Provides representation formats for some yaml values.
    /// </summary>
    public interface IYamlFormatSettings
    {
        /// <summary>
        /// Null format in yaml
        /// </summary>
        NullFormat NullFormat { get; }
    }
}