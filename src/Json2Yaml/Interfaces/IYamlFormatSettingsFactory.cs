using Json2Yaml.Cli;

namespace Json2Yaml.Interfaces
{
    /// <summary>
    /// This interface and its implementation separates CLI part from core conversion.
    /// </summary>
    public interface IYamlFormatSettingsFactory
    {
        IYamlFormatSettings Create(CommandOptions options);
    }
}