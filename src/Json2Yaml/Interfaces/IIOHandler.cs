using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Json2Yaml.Interfaces
{
    public interface IIOHandler
    {
        Task<string> ReadFileAsync(string path, CancellationToken cancellationToken);

        Task<Stream> ReadStreamAsync(string path, CancellationToken cancellationToken);

        Task WriteFileAsync(string path, string content, CancellationToken cancellationToken);
        Task WriteFileAsync(string path, Stream content, CancellationToken cancellationToken);

        bool IsSameExtension(string path, string extension);

        string[] FindFiles(string path, string extension);

        long GetFileSize(string path);

        bool IsInputRedirected { get; }

    }
}