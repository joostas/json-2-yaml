namespace Json2Yaml.Interfaces;

/// <summary>
/// Shows if stdin is redirected.
/// </summary>
public interface IConsoleRedirectionMarker
{
     /// <summary>
     /// Shows if stdin is redirected.
     /// </summary>
     bool IsInputRedirected { get; }

     /// <summary>
     /// Shows if program allows to update encoding.
     /// </summary>
     bool CanUpdateEncoding { get; }
}