using McMaster.Extensions.CommandLineUtils;

namespace Json2Yaml.Interfaces
{
    /// <summary>
    /// Initializes cli tool with options and their descriptions.
    /// </summary>
    public interface ICliInitializer
    {
        /// <summary>
        /// Initializes cli tool with options and their descriptions.
        /// </summary>
        void Initialize(CommandLineApplication app);
    }

}