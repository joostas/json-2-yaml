using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Json2Yaml.Interfaces;

/// <summary>
/// Handles remote io related operation.
/// Fetching remote resorces using HTTP.
/// </summary>
public interface IRemoteIOHandler
{
     Task<string> FetchJsonAsync(string url, bool skipContentTypeCheck, CancellationToken cancellationToken);

     Task<Stream> FetchStreamAsync(string url, bool skipContentTypeCheck, CancellationToken cancellationToken);
}