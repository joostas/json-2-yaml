using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Output
{
    public abstract class OutputWriter : IOutputWriter
    {
        protected readonly bool Verbose;
        public OutputWriter(bool verbose)
        {
            Verbose = verbose;
        }

        public void WriteError(Exception ex)
        {
            if (Verbose)
            {
                Console.Error.WriteLine(ex.ToString());
                Console.Error.WriteLine();
                Console.Error.WriteLine("Please check for existing issues https://gitlab.com/joostas/json-2-yaml/issues and register new one if needed here:");
                Console.Error.WriteLine("https://gitlab.com/joostas/json-2-yaml/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=");
            }
            else
            {
                Console.Error.WriteLine(ex.Message);
            }
        }

        public void WriteError(string message)
        {
            Console.Error.WriteLine(message);
        }

        public abstract void WriteInfo(string message);
        public abstract Task WriteOutputAsync(string output, string path, CancellationToken token);

        public abstract Task WriteOutputAsync(Stream output, string path, CancellationToken token);
    }
}