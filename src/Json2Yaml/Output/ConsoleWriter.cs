using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Json2Yaml.Output;

public class ConsoleOutputWriter : OutputWriter
{
    public ConsoleOutputWriter(bool verbose) : base(verbose)
    {
    }

    public override void WriteInfo(string message)
    {
        //Ignore
    }

    public override Task WriteOutputAsync(string output, string _, CancellationToken __)
    {
        using var ___ = new EncodingHelper();
        Console.WriteLine(output);
        return Task.CompletedTask;
    }

    public override async Task WriteOutputAsync(Stream output, string _, CancellationToken token)
    {
        using var __ = new EncodingHelper();
        using StreamReader sr = new(output, leaveOpen: true);
#if NET8_0_OR_GREATER
        Console.WriteLine(await sr.ReadToEndAsync(token));
#else
        Console.WriteLine(await sr.ReadToEndAsync());
#endif
    }

    private class EncodingHelper : IDisposable
    {
        private readonly Encoding _currentEncoding;
        public EncodingHelper()
        {
            _currentEncoding = Console.OutputEncoding;
            if (_currentEncoding != Encoding.UTF8)
            {
                Console.OutputEncoding = Encoding.UTF8;
            }
        }

        public void Dispose()
        {
            if (Console.OutputEncoding != _currentEncoding)
            {
                Console.OutputEncoding = _currentEncoding;
            }
        }
    }
}