using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Output
{
    public class FileOutputWriter : OutputWriter
    {
        private readonly IIOHandler _iohandler;

        public FileOutputWriter(IIOHandler iohandler, bool verbose) :base(verbose)
        {
            _iohandler = iohandler ?? throw new ArgumentNullException(nameof(iohandler));
        }

        public override void WriteInfo(string message)
        {
            Console.WriteLine(message);
        }

        public override async Task WriteOutputAsync(string output, string path, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("message", nameof(path));
            }
            await _iohandler.WriteFileAsync(path, output, cancellationToken);
        }

        public override async Task WriteOutputAsync(Stream output, string path, CancellationToken token)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("message", nameof(path));
            }
            await _iohandler.WriteFileAsync(path, output, token);
        }
    }
}