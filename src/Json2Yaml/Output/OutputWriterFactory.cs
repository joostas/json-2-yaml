using System;
using Autofac;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Output
{
    public class OutputWriterFactory : IOutputWriterFactory
    {
        private readonly ILifetimeScope _scope;
        public OutputWriterFactory(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public IOutputWriter Create(bool writeToConsole, bool verbose)
        {
            if (writeToConsole)
            {
                return _scope.Resolve<ConsoleOutputWriter>(new TypedParameter(typeof(bool), verbose));
            }
            else
            {
                return _scope.Resolve<FileOutputWriter>(new TypedParameter(typeof(bool), verbose));
            }
        }
    }
}