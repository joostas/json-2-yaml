using Json2Yaml.Interfaces;
using Json2Yaml.Yaml;

namespace Json2Yaml
{
    public class YamlFormatSettings : IYamlFormatSettings
    {
        public YamlFormatSettings(NullFormat nullFormat)
        {
            NullFormat = nullFormat;
        }

        public NullFormat NullFormat { get; }
    }
}