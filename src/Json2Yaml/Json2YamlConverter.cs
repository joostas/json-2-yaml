using System;
using System.IO;
using Json2Yaml.Interfaces;
using Json2Yaml.Json;

namespace Json2Yaml
{
    public class Json2YamlConverter : IJson2YamlConverter
    {
        private readonly IIOHandler _ioHandler;
        private readonly IJsonParserFactory _jsonParserFactory;
        private readonly IYamlProducer _yamlProducer;

        public Json2YamlConverter(
            IIOHandler handler,
            IJsonParserFactory jsonParserFactory,
            IYamlProducer yamlProducer)
        {
            _yamlProducer = yamlProducer ?? throw new ArgumentNullException(nameof(yamlProducer));
            _ioHandler = handler ?? throw new ArgumentNullException(nameof(handler));
            _jsonParserFactory = jsonParserFactory ?? throw new ArgumentNullException(nameof(jsonParserFactory));
        }

        public string Convert(string jsonInput, JsonParserSettings jsonParserSettings, IYamlFormatSettings yamlFormatSettings)
        {
            if (string.IsNullOrWhiteSpace(jsonInput))
            {
                return jsonInput;
            }

            if (yamlFormatSettings is null)
            {
                throw new ArgumentNullException(nameof(yamlFormatSettings));
            }

            return Convert((parser) => parser.ParseJson(jsonInput), jsonParserSettings, yamlFormatSettings);
        }

        public Stream Convert(Stream jsonInput, JsonParserSettings jsonParserSettings, IYamlFormatSettings yamlFormatSettings)
        {
            if (jsonInput is null)
            {
                throw new ArgumentNullException(nameof(jsonInput));
            }

            if (yamlFormatSettings is null)
            {
                throw new ArgumentNullException(nameof(yamlFormatSettings));
            }

            var jsonParser = _jsonParserFactory.Create(jsonParserSettings);
            object deserializedGraph = jsonParser.ParseJson(jsonInput);

            var stream = _yamlProducer.ProduceYamlStream(deserializedGraph, yamlFormatSettings);
            return stream;
        }

        private string Convert(Func<IJsonParser, object> parseJson, JsonParserSettings jsonParserSettings, IYamlFormatSettings yamlFormatSettings)
        {
            var jsonParser = _jsonParserFactory.Create(jsonParserSettings);
            object deserializedGraph = parseJson(jsonParser);
            string yaml = _yamlProducer.ProduceYaml(deserializedGraph, yamlFormatSettings);
            return yaml;
        }
    }
}