namespace Json2Yaml.Json;

public record JsonParserSettings(JsonFramework JsonFramework);
