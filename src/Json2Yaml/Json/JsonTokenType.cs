namespace Json2Yaml.Json;

/// <summary>
/// Represents different types of JsonToken
/// </summary>
public enum JsonTokenType
{
    None = 0,
    Object = 1,
    Array = 2,
    //Value it means simple string, int, number or null.
    Value = 3,
}
