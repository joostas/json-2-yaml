using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Nodes;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Json;

public class SystemTextJsonParser : IJsonParser
{
    private static readonly int s_recursionLimit = 10;

    public object ParseJson(string json)
    {
        if (string.IsNullOrWhiteSpace(json))
        {
            throw new ArgumentException("json cannot be empty", nameof(json));
        }
        try
        {
            var jToken = JsonNode.Parse(json, documentOptions: new() { CommentHandling = JsonCommentHandling.Skip });
            return ConvertJTokenToObject(jToken, 0);
        }
        catch (JsonException jsonEx)
        {
            throw new Json2YamlException(
                $"{jsonEx.Message} {Environment.NewLine}Error occured while reading JSON text. Please check if given json is valid",
                jsonEx);
        }
    }

    public object ParseJson(Stream jsonStream)
    {
        if (jsonStream is null)
        {
            throw new ArgumentNullException(nameof(jsonStream));
        }

        try
        {
            var jToken = JsonNode.Parse(jsonStream, documentOptions: new() {CommentHandling = JsonCommentHandling.Skip});
            return ConvertJTokenToObject(jToken, 0);
        }
        catch (JsonException jsonEx)
        {
            throw new Json2YamlException(
                $"{jsonEx.Message} {Environment.NewLine}Error occured while reading JSON text. Please check if given json is valid",
                jsonEx);
        }
    }

    private object ConvertJTokenToObject(JsonNode token, int level)
    {
        if (level > s_recursionLimit)
            throw new Json2YamlException($"Json structure is not supported: nested level in array is too deep. ({level}).");
        return token switch
        {
            JsonValue val => SystemTextJsonParser.ParseValue(val),
            JsonArray arr => arr.Select(o => ConvertJTokenToObject(o, level + 1)).ToList(),
            JsonObject obj => obj.AsObject().ToDictionary(x => x.Key, x => ConvertJTokenToObject(x.Value, level)),
            null => null,
            _ => throw new InvalidOperationException("Unexpected token: " + token)
        };
    }

    private static object ParseValue(JsonValue token)
    {
        var elem = (JsonElement)token.GetValue<object>();
        return elem.ValueKind switch
        {
            JsonValueKind.String => elem.GetString(),
            JsonValueKind.Number => elem.GetDecimal(),
            JsonValueKind.False => false,
            JsonValueKind.True => true,
            JsonValueKind.Null => null,
            _ => throw new NotSupportedException(),
        };
    }
}
