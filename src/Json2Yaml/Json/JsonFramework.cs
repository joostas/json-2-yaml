﻿using System;

namespace Json2Yaml.Json;

public struct JsonFramework
{
    private string Value { get; }

    public JsonFramework(string value)
    {
        Value = value?.Trim().ToLower() switch
        {
            "system" => value?.Trim().ToLower(),
            "newtonsoft" => value?.Trim().ToLower(),
            _ => throw new ArgumentException($"Value '{value}' is not supported. Please use one of these: {SupportedFrameworks}"),
        };
    }

    public override string ToString() => Value;
    public static implicit operator string(JsonFramework value) => value.Value;

    public static JsonFramework Default => new("system");
    public static string SupportedFrameworks =>
    "system|newtonsoft";
}
