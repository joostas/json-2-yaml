using Json2Yaml.Interfaces;

namespace Json2Yaml.Json;

public class JsonParserFactory : IJsonParserFactory
{

    public IJsonParser Create(JsonParserSettings settings) => settings.JsonFramework.ToString() switch
    {
       "newtonsoft" => new NewtonsoftJsonParser(),
        _ => new SystemTextJsonParser(),
    };
}
