using System;
using System.IO;
using System.Linq;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Json2Yaml.Json;

public class NewtonsoftJsonParser : IJsonParser
{
    private static readonly int s_recursionLimit = 10;

    public object ParseJson(string json)
    {
        if (string.IsNullOrWhiteSpace(json))
        {
            throw new ArgumentException("json cannot be empty", nameof(json));
        }
        try
        {
            var settings = new JsonSerializerSettings()
            {
                FloatParseHandling = FloatParseHandling.Decimal
            };
            var jToken = JsonConvert.DeserializeObject<JToken>(json, settings);
            return ConvertJTokenToObject(jToken, 0);
        }
        catch (JsonReaderException jsonEx)
        {
            throw new Json2YamlException(
                $"{jsonEx.Message} {Environment.NewLine}Error occured while reading JSON text. Please check if given json is valid",
                jsonEx);
        }
    }

    public object ParseJson(Stream jsonStream)
    {
        using StreamReader sr = new StreamReader(jsonStream);
        //sr.BaseStream.Position = 0;
        using JsonReader reader = new JsonTextReader(sr);
        try
        {
            var settings = new JsonSerializerSettings()
            {
                FloatParseHandling = FloatParseHandling.Decimal
            };
            JsonSerializer serializer = new JsonSerializer();
            serializer.FloatParseHandling = FloatParseHandling.Decimal;
            var jToken = serializer.Deserialize<JToken>(reader);
            return ConvertJTokenToObject(jToken, 0);
        }
        catch (JsonReaderException jsonEx)
        {
            throw new Json2YamlException(
                $"{jsonEx.Message} {Environment.NewLine}Error occured while reading JSON text. Please check if given json is valid",
                jsonEx);
        }
    }
    
    private object ConvertJTokenToObject(JToken token, int level)
    {
        if (level > s_recursionLimit)
            throw new Json2YamlException($"Json structure is not supported: nested level in array is too deep. ({level}).");
        return token switch
        {
            JValue _ => NewtonsoftJsonParser.ParseValue(token),
            JArray _ => token.AsEnumerable().Select(o => ConvertJTokenToObject(o, level + 1)).ToList(),
            JObject _ => token.AsEnumerable().Cast<JProperty>().ToDictionary(x => x.Name, x => ConvertJTokenToObject(x.Value, level)),
            _ => throw new InvalidOperationException("Unexpected token: " + token)
        };
    }

    private static object ParseValue(JToken token) => token.Type switch
    {
        JTokenType.Integer => (decimal)token,
        JTokenType.Float => (decimal)token,
        JTokenType.String => (string)token,
        JTokenType.Date => (DateTime)token,
        JTokenType.Null => null,
        JTokenType.Boolean => (bool)token,
        _ => (string)token
    };
}
