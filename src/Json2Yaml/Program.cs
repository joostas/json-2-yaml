﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core;
using Json2Yaml.Interfaces;
using Json2Yaml.Utils;

[assembly: InternalsVisibleTo("Json2Yaml.Integration.Test")]
namespace Json2Yaml
{
    internal class Program
    {
        /// <summary>
        /// Main enty point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static async Task<int> Main(string[] args)
        {
            return await StartAsync(args);
        }

        /// <summary>
        /// Entry point for integration tests.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="module">Used to pass mock implementations for integration testing</param>
        /// <returns></returns>
        internal static async Task<int> StartAsync(string[] args, IModule module = null, CancellationToken cancelationToken = default)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            bool canWriteToConsole = false;
            try
            {
                var container = DependencyRegistrer.ConfigureDependencies(module);
                int returnCode = 0;
                using(var scope = container.BeginLifetimeScope())
                {
                    //initial value which will be used before end of parsing of arguments.
                    IOutputWriter output = scope.Resolve<IOutputWriter>();
                    try
                    {
                        var json2YamlCli = scope.Resolve<IJson2YamlCli>();
                        json2YamlCli.OutputInitialized += 
                            (sender, evArgs) => output = ((IJson2YamlCli)sender).Output; //redirect output if needed
                        json2YamlCli.Initialize();
                        json2YamlCli.ParseArguments(args);
                        canWriteToConsole = json2YamlCli.CanWriteInfoToConsole;
                        returnCode = await json2YamlCli.RunAsync(cancelationToken);
                        //after successfull run we maybe want to prevent more messages.
                        canWriteToConsole = json2YamlCli.CanWriteInfoToConsole;
                    }
                    catch (Exception ex)
                    {
                        output.WriteError(ex);
                        return 1;
                    }
                    return returnCode;
                }
            }
            catch(Exception ex)
            {
                Console.Error.WriteLine(ex.Message); //This only for catching IOC exceptions.
                return 1;
            }
            finally
            {
                stopWatch.Stop();
                if (canWriteToConsole)
                {
                    Console.WriteLine($"json2yaml tool run {stopWatch.ElapsedMilliseconds} ms.");
                }
            }
        }
    }
}