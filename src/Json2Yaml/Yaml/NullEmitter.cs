using System;
using Json2Yaml.Interfaces;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.EventEmitters;

namespace Json2Yaml.Yaml
{
    /// <summary>
    /// This emitter emits null based on provided format instead of default empty value
    /// </summary>
    public class NullEmitter : ChainedEventEmitter
    {
        private readonly string _nullFormat;

        public NullEmitter(IEventEmitter nextEmitter, IYamlFormatSettings yamlFormatSettings)
            : base(nextEmitter) 
        {
            if (yamlFormatSettings is null)
            {
                throw new ArgumentNullException(nameof(yamlFormatSettings));
            }

            _nullFormat = yamlFormatSettings.NullFormat;
        }

        public override void Emit(ScalarEventInfo eventInfo, IEmitter emitter)
        {
            if (eventInfo.Source.Value == null)
            {
                var tag = "tag:yaml.org,2002:null"; //Based on this code: https://github.com/aaubry/YamlDotNet/blob/d856dbc52a1b9d7bdd7a07f774637a018e8fb907/YamlDotNet/Core/Emitter.cs#L277-L287
                emitter.Emit(new Scalar(tag, _nullFormat));
            }
            else
            {
                base.Emit(eventInfo, emitter);
            }
        }
    }
}