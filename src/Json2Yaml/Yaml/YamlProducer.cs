using System;
using System.IO;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;
using YamlDotNet.Core;
using YamlDotNet.Serialization;

namespace Json2Yaml.Yaml
{
    public class YamlProducer : IYamlProducer
    {
        public string ProduceYaml(object json, IYamlFormatSettings yamlFormatSettings)
        {
            if (yamlFormatSettings is null)
            {
                throw new ArgumentNullException(nameof(yamlFormatSettings));
            }

            var serializer = new SerializerBuilder()
                .WithEventEmitter(nextEmitter => new NullEmitter(nextEmitter, yamlFormatSettings))
                .Build();
            
            try
            {
                string yaml = serializer.Serialize(json);
                return yaml;
            }
            catch (YamlException yex)
            {
                throw new Json2YamlException($"{yex.Message}{Environment.NewLine}Exception occured during yaml producing", yex);
            }
        }

        //accept text writer and write directly to it.
        public Stream ProduceYamlStream(object json, IYamlFormatSettings yamlFormatSettings)
        {
            if (yamlFormatSettings is null)
            {
                throw new ArgumentNullException(nameof(yamlFormatSettings));
            }

            var serializer = new SerializerBuilder()
                .WithEventEmitter(nextEmitter => new NullEmitter(nextEmitter, yamlFormatSettings))
                .Build();
            
            try
            {
                MemoryStream stream = new MemoryStream();
                using StreamWriter streamWriter = new StreamWriter(stream, leaveOpen: true);
                serializer.Serialize(streamWriter, json);
                streamWriter.Flush();
                stream.Seek(0, SeekOrigin.Begin);
                return stream;
            }
            catch (YamlException yex)
            {
                throw new Json2YamlException($"{yex.Message}{Environment.NewLine}Exception occured during yaml producing", yex);
            }
        }
    }
}