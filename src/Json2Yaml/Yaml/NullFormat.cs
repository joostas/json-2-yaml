using System;
namespace Json2Yaml.Yaml
{
    /// <summary>
    /// How to represent null in yaml.
    /// In yaml null can be represented in different ways See https://yaml.org/spec/1.2/spec.html#id2805071
    /// This enum holds all possible variants.
    /// </summary>
    public struct NullFormat
    {
        private string Value {get;}

        public NullFormat(string value)
        {
            switch (value)
            {
                case "empty":
                    Value = string.Empty;
                    break;
                case "":
                case "null":
                case "Null":
                case "NULL":
                case "~":
                    Value = value;
                    break;
                default:
                    throw new ArgumentException($"Value '{value}' is not supported. Please use one of these: {NullFormat.SupportedFormats}");
            }
        }

        public override string ToString() => Value != string.Empty ? Value : "empty";

        public static string SupportedFormats => "null|Null|NULL|~|empty";

        public static NullFormat Default => new NullFormat("null");

        public static implicit operator string(NullFormat value) => value.Value;
    }
}