﻿using Json2Yaml.Interfaces;
using Json2Yaml.Json;
using Json2Yaml.Yaml;
using McMaster.Extensions.CommandLineUtils;

namespace Json2Yaml.Cli;

/// <summary>
/// Initializes cli tool with options and their descriptions.
/// </summary>
public class CliInitializer : ICliInitializer
{
    private readonly IVersionProvider _versionProvider;
    private readonly Constants _constants;
    public CliInitializer(IVersionProvider versionProvider, Constants constants)
    {
        _versionProvider = versionProvider ??
            throw new System.ArgumentNullException(nameof(versionProvider));
        _constants = constants ??
            throw new System.ArgumentNullException(nameof(constants));
    }

    /// <summary>
    /// Initializes cli tool with options and their descriptions.
    /// </summary>
    /// <param name="app">cli application to initialize</param>
    /// <exception cref="ArgumentNullException">IThrows i provided application is null.</exception>
    public void Initialize(CommandLineApplication app)
    {
        if (app is null)
        {
            throw new System.ArgumentNullException(nameof(app));
        }

        app.Name = "json2yaml";
        app.FullName = "json2yaml converter";
        app.Description = "json2yaml converter reads json input and produces converted yaml output";
        app.ExtendedHelpText = "Welcome to json2yaml converter. Run dotnet json2yaml to convert json to yaml";

        app.UnrecognizedArgumentHandling = UnrecognizedArgumentHandling.StopParsingAndCollect;
        app.Option(
            "-i|--input",
            "Path to json input file. E.g. `-i:file.json` (or `-i=\"file.json\"`). If empty value provided (`-i`), converter will search current directory for *.json file.",
            CommandOptionType.SingleOrNoValue);
        app.Option(
            "-u|--url",
            "HTTP endpoint to fetch json input from.",
            CommandOptionType.SingleOrNoValue);
        app.Option(
            "--skip-content-type-check",
            "Flag specifies if http response Content-Type header check can be skipped. Only in effect when -u option is provided.",
            CommandOptionType.NoValue
        );
        app.Option(
            "-c|--console-output",
            "Flag specifies if converted yaml should be printed directly to console. (In this case -o option is ignorred.)",
            CommandOptionType.NoValue);
        app.Option(
            "-o|--output",
            "Path to output yaml file. E.g. `-o:file.yaml (or -o=\"file.yaml\")`. If omitted or empty value provided ('-o') converter will output to file named like input file or the last segment of url, but with .yaml extension. Required if input is read from stdin.",
            CommandOptionType.SingleOrNoValue);
        app.Option<int?>(
            "-m|--max-size",
            $"Specifies size limit for file to be converted. E.g. `-m:10`. If omitted, converter will skip file larger that {_constants.MaxSizeMbDefault} MB. Respected only when used together with -i option.",
            CommandOptionType.SingleOrNoValue);
        app.Option(
            "--format-null-as",
            $"Yaml can represent null in different ways. Supported values: {NullFormat.SupportedFormats}. Default is \"{NullFormat.Default}\".",
            CommandOptionType.SingleOrNoValue
        );
        app.Option(
            "--json-framework",
            $"framework to use for json parser. Supported values: {JsonFramework.SupportedFrameworks}. Default is \"{JsonFramework.Default}\".",
            CommandOptionType.SingleOrNoValue
        );
        app.Option(
            "--version",
            "Shows tool version. If more options are provided this option is ignored.",
            CommandOptionType.NoValue
        );
        app.Option(
            "-v|--verbose",
            "Verbose output. Shows more details on errors.",
            CommandOptionType.NoValue
        );
        app.HelpOption();
        app.ShortVersionGetter = () => _versionProvider.ReadProjectVersion();
    }
}
