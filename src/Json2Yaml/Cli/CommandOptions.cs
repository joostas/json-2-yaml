using System;
using System.Linq;
using System.Reflection;
using Json2Yaml.Json;
using Json2Yaml.Yaml;

namespace Json2Yaml.Cli;

public class CommandOptions
{
    public OptionInfo<string> Input {get; set;}
    
    public OptionInfo<string> Output {get; set;}

    public OptionInfo<string> Url {get; set;}

    /// <summary>
    /// Flag marks if check of http response header Content-Type can be skipped
    /// </summary>
    public OptionInfo<bool> SkipContentTypeCheck {get; set;}

    public OptionInfo<int?> MaxSize {get; set;}

    public OptionInfo<bool> ConsoleOutput {get; set;}

    public OptionInfo<bool> Version {get; set;}

    /// <summary>
    /// Verbose output (more details on exceptions) Decorative.
    /// </summary>
    public OptionInfo<bool> Verbose {get; set;}

    /// <summary>
    /// How to represent null in yaml output.
    /// </summary>
    public OptionInfo<NullFormat> NullFormat {get; set;}

    /// <summary>
    /// Which parsing framework to use: System.Text.Json or Newtonsoft.Json
    /// </summary>
    public OptionInfo<JsonFramework> JsonFramework { get; set; }

    /// <summary>
    /// Shows if any option except decorating options are defined.
    /// </summary>
    ///<remarks>Options is decorating if use of it alone doesn't influence tool output (yaml). e.g. json2yaml --verbose</remarks>
    public bool IsAnyNonDecorativeDefined() => AnyPropertiesDefined((p) => p.Name != nameof(Verbose));

    public bool IsOnlyVersionProvided() => 
        Version?.IsDefined == true && Version.HasValue &&
        !AnyPropertiesDefined((p) => p.Name != nameof(Version));

    private bool AnyPropertiesDefined(Func<PropertyInfo, bool> filter)
    {
        var options = GetType()
            .GetProperties(BindingFlags.Instance | BindingFlags.Public)
            .Where(p => p.GetValue(this) is OptionInfo && filter(p)).Select(p => (OptionInfo)p.GetValue(this));
        return options.Any(o => o.IsDefined);
    }
}
