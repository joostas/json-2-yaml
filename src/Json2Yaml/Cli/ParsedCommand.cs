using System.Collections.Generic;
using McMaster.Extensions.CommandLineUtils;

namespace Json2Yaml.Cli;

public record ParsedCommand(IReadOnlyList<string> RemainingArguments, List<ParsedOption> Options);

public record ParsedOption(string ShortName, string LongName, CommandOptionType OptionType, bool HasValue, IReadOnlyList<string> Values, string Value);
