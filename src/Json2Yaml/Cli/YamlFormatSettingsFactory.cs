using Json2Yaml.Interfaces;
using Json2Yaml.Yaml;

namespace Json2Yaml.Cli
{
    public class YamlFormatSettingsFactory : IYamlFormatSettingsFactory
    {
        public IYamlFormatSettings Create(CommandOptions options)
        {
            if (options is null)
            {
                throw new System.ArgumentNullException(nameof(options));
            }
            var nullFormatOption = options.NullFormat;
            var nullFormat = nullFormatOption.HasValue ? nullFormatOption.Value : NullFormat.Default;
            return new YamlFormatSettings(nullFormat);
        }
    }
}