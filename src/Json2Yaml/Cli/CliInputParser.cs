using System;
using System.Collections.Generic;
using System.Linq;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;
using Json2Yaml.Json;
using Json2Yaml.Yaml;

namespace Json2Yaml.Cli;

/// <summary>
/// Adapter to work with command input arguments parsed by CommandLineUtils lib.
/// </summary>
public class CliInputParser : ICliInputParser
{
    private readonly ICliApplicationHelper _cliHelper;

    /// <inheritdoc/>
    public CliInputParser(ICliApplicationHelper cliHelper)
    {
        _cliHelper = cliHelper ??
            throw new ArgumentNullException(nameof(cliHelper));
    }

    /// <summary>
    /// Reads comman arguments input from structure provided by CommandLineUtils tool after argument parsing.
    /// </summary>
    /// <param name="parsedCommand">CommandLineUtils framework structure created after parsing command line arguments.</param>
    /// <returns>Own structure of parsed arguments. See <see cref="CommandOptions"/>.</returns>
    /// <exception cref="ArgumentNullException">Argument null exception.</exception>
    public CommandOptions ParseArguments(ParsedCommand parsedCommand)
    {
        if (parsedCommand is null)
            throw new ArgumentNullException(nameof(parsedCommand));
        if (parsedCommand.RemainingArguments.Any())
        {
            ThrowUnrecognizedArgumentsException(parsedCommand);
        }
        var options = parsedCommand.Options;
        var commandOptions = new CommandOptions()
        {
            Input = _cliHelper.GetValueInfo<string>(options, "i"),
            Output = _cliHelper.GetValueInfo<string>(options, "o"),
            Url = _cliHelper.GetValueInfo<string>(options, "u"),
            SkipContentTypeCheck = _cliHelper.GetValueInfo<bool>(options, "skip-content-type-check"),
            MaxSize = _cliHelper.GetValueInfo<int?>(options, "m"),
            ConsoleOutput = _cliHelper.GetValueInfo<bool>(options, "c"),
            Version = _cliHelper.GetValueInfo<bool>(options, "version"),
            Verbose = _cliHelper.GetValueInfo<bool>(options, "v"),
            NullFormat = _cliHelper.GetValueInfo(options, "format-null-as", (o) => new NullFormat(o)),
            JsonFramework = _cliHelper.GetValueInfo(options, "json-framework", (o) => new JsonFramework(o))
        };
        ValidateOptionsThatCannotBeEmpty(commandOptions);
        return commandOptions;
    }

    // This method is needed, because cMaster.Extensions.CommandLineUtils framework
    // incorrectly handles SingleValue option which misses value.
    // see https://github.com/natemcmaster/CommandLineUtils/issues/250
    private static void ValidateOptionsThatCannotBeEmpty(CommandOptions parsedOptions)
    {
        if (parsedOptions.MaxSize.IsDefined && !parsedOptions.MaxSize.HasValue)
        {
            throw new Json2YamlException("Ups. Failed to parse input arguments. Option -m|--max-size cannot be empty.");
        }
        if (parsedOptions.Url.IsDefined && !parsedOptions.Url.HasValue)
        {
            throw new Json2YamlException("Ups. Failed to parse input arguments. Option -u|--url cannot be empty.");
        }
        if (parsedOptions.NullFormat.IsDefined && !parsedOptions.NullFormat.HasValue)
        {
            throw new Json2YamlException($"Ups. Failed to parse input arguments. Option --format-null-as cannot be empty. Use one of values: {NullFormat.SupportedFormats}");
        }
        if (parsedOptions.JsonFramework.IsDefined && !parsedOptions.JsonFramework.HasValue)
        {
            throw new Json2YamlException($"Ups. Failed to parse input arguments. Option --json-framework cannot be empty. Use one of values: {JsonFramework.SupportedFrameworks}");
        }
    }

    /// <summary>
    /// Reads info from tool input to decide if it is permitted to write to console (stdin).
    /// </summary>
    /// <exception cref="ArgumentNullException">Throws if options is null</exception>
    public bool CanWriteInfoToConsole(List<ParsedOption> options)
    {
        if (options is null)
        {
            throw new ArgumentNullException(nameof(options));
        }
        return !_cliHelper.GetValueInfo(options, "c").isDefined;
    }

    public bool IsVerbose(List<ParsedOption> options)
    {
        if (options is null)
        {
            throw new ArgumentNullException(nameof(options));
        }
        return _cliHelper.GetValueInfo(options, "v").isDefined;
    }

    private static void ThrowUnrecognizedArgumentsException(ParsedCommand parsedCommand)
    {
        var unrecognizedArguments = string.Join(" ", parsedCommand.RemainingArguments);
        //TODO Maybe possible to write better hint
        var hint = "Hint: use argument in this format -i:file.json or (-i=\"file.json\"), but not -i file.json";
        throw new Json2YamlException(
            $"Command contains unrecognized arguments: {unrecognizedArguments}{Environment.NewLine}{hint}");
    }
}
