namespace Json2Yaml.Cli
{
    public abstract class OptionInfo
    {
        public abstract bool IsDefined {get; set;}
    }
}