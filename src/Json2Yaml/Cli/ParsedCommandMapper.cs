using System;
using System.Linq;
using Boxed.Mapping;
using McMaster.Extensions.CommandLineUtils.Abstractions;

namespace Json2Yaml.Cli;

public class ParsedCommandMapper : IImmutableMapper<ParseResult, ParsedCommand>
{
    public ParsedCommand Map(ParseResult source)
    {
        if (source is null)
        {
            throw new ArgumentException(null, nameof(source));
        }
        var parsedOptions = source.SelectedCommand.Options.Select(o =>
            new ParsedOption(o.ShortName, o.LongName, o.OptionType, o.HasValue(), o.Values, o.Value()));
        return new ParsedCommand(source.SelectedCommand.RemainingArguments, parsedOptions.ToList());
    }
}