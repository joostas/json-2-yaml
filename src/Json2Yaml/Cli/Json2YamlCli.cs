using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Boxed.Mapping;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;
using Json2Yaml.Utils;
using McMaster.Extensions.CommandLineUtils;
using McMaster.Extensions.CommandLineUtils.Abstractions;

namespace Json2Yaml.Cli;

public class Json2YamlCli : IJson2YamlCli
{
    private readonly CommandLineApplication _app;
    private bool _isInitialized = false;
    private bool _areArgumentsParsed = false;
    private readonly IJson2YamlConverter _json2YamlConverter;
    private readonly IIOHandler _ioHandler;
    private readonly IInputReaderFactory _inputFactory;
    private readonly IOutputWriterFactory _outputWriterFactory;
    private readonly ICliInitializer _cliInitializer;
    private CommandOptions _parsedOptions;
    private readonly ICliInputParser _cliParser;
    private readonly Func<CommandOptions, ICliOptionsInterpreter> _cliInterpreterFactory;
    private readonly IImmutableMapper<ParseResult, ParsedCommand> _mapper;

    public Json2YamlCli(
        IJson2YamlConverter json2YamlConverter,
        IIOHandler ioHandler,
        IInputReaderFactory inputFactory,
        IOutputWriterFactory outputWriterFactory,
        ICliInitializer cliInitializer,
        ICliInputParser cliParser,
        Func<CommandOptions, ICliOptionsInterpreter> cliInterpreterFactory,
        IImmutableMapper<ParseResult, ParsedCommand> mapper)
    {
        _json2YamlConverter = json2YamlConverter ??
            throw new ArgumentNullException(nameof(json2YamlConverter));
        _ioHandler = ioHandler ??
            throw new ArgumentNullException(nameof(ioHandler));
        _inputFactory = inputFactory ??
            throw new ArgumentNullException(nameof(inputFactory));
        _outputWriterFactory = outputWriterFactory ??
            throw new ArgumentNullException(nameof(outputWriterFactory));
        _cliInitializer = cliInitializer ??
            throw new ArgumentNullException(nameof(cliInitializer));
        _cliParser = cliParser ??
            throw new ArgumentNullException(nameof(cliParser));
        _cliInterpreterFactory = cliInterpreterFactory ?? 
            throw new ArgumentNullException(nameof(cliInterpreterFactory));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _app = new CommandLineApplication();
    }

    public event EventHandler OutputInitialized;

    public bool CanWriteInfoToConsole {get; private set;}

    public IOutputWriter Output {get; private set;}

    public void Initialize()
    {
        _cliInitializer.Initialize(_app);
        _app.OnParsingComplete(ParseArguments);
        _app.OnExecuteAsync(ExecuteAsync);
        _isInitialized = true;
    }

    public void ParseArguments(string[] args)
    {
        if (!_isInitialized)
        {
            throw new InvalidOperationException("Cannot parse arguments while app is not initialized.");
        }
        try
        {
            _app.Parse(args);
        }
        catch (InvalidOperationException invex)
        {
            throw new Json2YamlException("Ups. Failed to parse input arguments.", invex);
        }
        catch (CommandParsingException pex)
        {
            throw new Json2YamlException($"Ups. Failed to parse input arguments. {pex.Message}", pex);
        }
    }

    public Task<int> RunAsync(CancellationToken cancellationToken = default)
    {
        if (!_isInitialized || !_areArgumentsParsed)
        {
            throw new InvalidOperationException("Cannot run tool when tool is not initialized and (or) arguments aren't parsed.");
        }
        return _app.ExecuteAsync(null, cancellationToken);
    }

    private void ParseArguments(ParseResult parseResults)
    {
        if (_areArgumentsParsed)
        {
            return; //don't need to parse one more time.
        }
        ParsedCommand parsedCommand = _mapper.Map(parseResults);
        InitializeOutputWriter(parsedCommand.Options);
        _parsedOptions = _cliParser.ParseArguments(parsedCommand);
        _areArgumentsParsed = true;
    }

    private void  InitializeOutputWriter(List<ParsedOption> options)
    {
        //Info stands for - additional info (e.g. how long tool executed) and results stands for conversion result (yaml).
        CanWriteInfoToConsole = _cliParser.CanWriteInfoToConsole(options);
        var verbose = _cliParser.IsVerbose(options);
        var writeResultsToConsole = !CanWriteInfoToConsole; 
        Output = _outputWriterFactory.Create(writeResultsToConsole, verbose);
        RaiseOutputInitializedEvent();
    }

    private async Task<int> ExecuteAsync(CancellationToken cancellationToken = default)
    {
        if (!_parsedOptions.IsAnyNonDecorativeDefined())
        {
            return HandleEmptyArguments();
        }
        var interpreter = _cliInterpreterFactory(_parsedOptions);
        var executionContext = interpreter.PrepareForConversion();
        if (!executionContext.PerformConversion)
        {
            switch(executionContext.MessageType)
            {
                case MessageType.Info:
                    Output?.WriteInfo(executionContext.Message);
                    if (CanWriteInfoToConsole && executionContext.ShouldBeOnlyMessage)
                        CanWriteInfoToConsole = false;
                    return 0;
                case MessageType.Error:
                    Output?.WriteError(executionContext.Message);
                    return 1;
            }
        }
        var inputSettings = executionContext.InputSettings;
        var outputSettings = executionContext.OutputSettings;
        //Conversion happens here.
        var inputReader = _inputFactory.Create(inputSettings.InputType, inputSettings.SkipContentTypeCheck);
        //var jsonInput = await inputReader.ReadJsonAsync(inputSettings.InputFileName, cancellationToken);
        using var jsonStream = await inputReader.OpenStreamAsync(inputSettings.InputFileName, cancellationToken);
        using Stream yamlStream = _json2YamlConverter.Convert(jsonStream, inputSettings.JsonSettings, outputSettings.YamlFormatSettings);
        await Output.WriteOutputAsync(yamlStream, outputSettings.OutputFileName, cancellationToken);
        Output?.WriteInfo("Conversion ended successfully.");
        Output?.WriteInfo($"Input read from: `{inputSettings.InputFileName}`");
        Output?.WriteInfo($"Outputed to: `{outputSettings.OutputFileName}`.");
        return 0;
    }

    private int HandleEmptyArguments()
    {
        if (_ioHandler.IsInputRedirected)
        {
            Output?.WriteError("Missing output information. Provide it using --output option or --console-output flag.");
            return 1;
        }
        else
        {
            _app.ShowHelp();
            return 0;
        }
    }

    private void RaiseOutputInitializedEvent() => OutputInitialized?.Invoke(this, new EventArgs());

}
