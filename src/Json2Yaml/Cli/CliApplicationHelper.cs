using System;
using System.Collections.Generic;
using System.Linq;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;
using McMaster.Extensions.CommandLineUtils;

namespace Json2Yaml.Cli;

public class CliApplicationHelper : ICliApplicationHelper
{
    public (bool isDefined, string value) GetValueInfo(List<ParsedOption> options, string optionName)
    {
        if (options is null)
        {
            throw new ArgumentNullException(nameof(options));
        }
        if (string.IsNullOrWhiteSpace(optionName))
        {
            throw new ArgumentException(null, nameof(optionName));
        }
        var option = options.FirstOrDefault(o => o.ShortName == optionName ||
            o.LongName == optionName);
        bool hasValue;
        string value;
        if (option?.OptionType == CommandOptionType.NoValue)
        {
            hasValue = option?.Values.Count > 0;
            value = hasValue.ToString();
        }
        else
        {
            hasValue = (option?.HasValue).GetValueOrDefault();
            value = option?.Value;
        }
        return (hasValue, value);
    }

    public OptionInfo<T> GetValueInfo<T>(List<ParsedOption> options, string optionName) =>
        GetValueInfo<T>(options, optionName, null);

    public OptionInfo<T> GetValueInfo<T>(List<ParsedOption> options, string optionName, Func<string, T> converter)
    {
        var (isDefined, value) = GetValueInfo(options, optionName);
        if (!isDefined)
        {
            return new OptionInfo<T>(isDefined, default);
        }
        else if (value == default) //This else prevents from convertion failure for null values
        {
            return new OptionInfo<T>(isDefined, default);
        }
        T convertedValue;
        try
        {
            if (converter != null)
            {
                convertedValue = converter(value);
            }
            else
            {
                var type = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
                convertedValue = (T)Convert.ChangeType(value, type);
            }
        }
        catch(InvalidCastException icex)
        {
            throw new Json2YamlException($"Option '{optionName}' parsing: conversion from string to {typeof(T).Name} is not supported", icex);
        }
        catch (FormatException fex)
        {
            throw new Json2YamlException($"Option '{optionName}' parsing: wrong format.", fex);
        }
        catch(ArgumentException aex)
        {
            throw new Json2YamlException($"Option '{optionName}' parsing error. {aex.Message}", aex);
        }
        catch(Exception ex)
        {
            throw new Json2YamlException($"Option '{optionName}' parsing error.", ex);
        }
        return new OptionInfo<T>(isDefined, convertedValue);
    }
}