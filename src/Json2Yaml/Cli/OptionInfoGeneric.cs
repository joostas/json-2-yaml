using System.Collections.Generic;

namespace Json2Yaml.Cli
{
    public class OptionInfo<T> : OptionInfo
    {
        public OptionInfo()
        {
        }

        public OptionInfo(bool isDefined, T value)
        {
            IsDefined = isDefined;
            Value = value;
        }

        public override bool IsDefined {get; set;}

        public T Value {get; set;}

        public bool HasValue => !EqualityComparer<T>.Default.Equals(Value, default); //More details: https://stackoverflow.com/questions/65351/null-or-default-comparison-of-generic-argument-in-c-sharp
    }
}