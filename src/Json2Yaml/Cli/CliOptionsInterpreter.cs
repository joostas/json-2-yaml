﻿using System;
using System.IO;
using System.Text;
using Json2Yaml.Input;
using Json2Yaml.Interfaces;
using Json2Yaml.Json;
using Json2Yaml.Utils;
using src.Json2Yaml.Utils;

namespace Json2Yaml.Cli;

public class CliOptionsInterpreter : ICliOptionsInterpreter
{
    private readonly CommandOptions _options;
    private readonly IUrlHelper _urlHelper;
    private readonly IIOHandler _ioHandler;
    private readonly IVersionProvider _versionProvider;
    private readonly IYamlFormatSettingsFactory _formatSettingsFactory;
    private readonly int _maxSizeMbDefault;

    public CliOptionsInterpreter(
        CommandOptions options,
        IUrlHelper urlHelper,
        IIOHandler ioHandler,
        Constants constants,
        IVersionProvider versionProvider,
        IYamlFormatSettingsFactory formatSettingsFactory)
    {
        _options = options ?? throw new ArgumentNullException(nameof(options));
        _urlHelper = urlHelper ?? throw new ArgumentNullException(nameof(urlHelper));
        _ioHandler = ioHandler ?? throw new ArgumentNullException(nameof(ioHandler));
        _versionProvider = versionProvider ?? throw new ArgumentNullException(nameof(versionProvider));
        _formatSettingsFactory = formatSettingsFactory ?? throw new ArgumentNullException(nameof(formatSettingsFactory));
        if (constants is null)
        {
            throw new ArgumentNullException(nameof(constants));
        }
        _maxSizeMbDefault = constants.MaxSizeMbDefault;
    }

    public (bool passed, string errorMessage) AreCorrectInputOptionsProvided()
    {
        if (_options.Input.IsDefined && _options.Url.IsDefined)
        {
            return (false, "Please choose either -i|--input or -u|--url option, but not both together.");
        }
        return (true, string.Empty);
    }

    public ConversionContext PrepareForConversion()
    {
        if (_options.IsOnlyVersionProvided())
        {
            return CreateInfoContext(
                _versionProvider.ReadProjectVersion(),
                shouldBeOnlyMessage: true);
        }
        var (passed, errorMessage) = AreCorrectInputOptionsProvided();
        if (!passed)
        {
            return CreateErrorContext(errorMessage);
        }
        InputType inputType = InputType.Console;
        string inputName = "stdin";
        bool skipContentTypeCheck = _options.SkipContentTypeCheck.IsDefined;
        if (_options.Input.IsDefined)
        {
            inputType = InputType.File;
            var (successful, result) = EvaluateFileInput();
            if (!successful) //it means we cannot move further
                return CreateErrorContext(result);
            inputName = result;
        }
        else if (_options.Url.IsDefined)
        {
            inputType = InputType.Http;
            var (sucessful, result) = EvaluateHttpEndpointInput();
            if (!sucessful) //it means we cannot move further
                return CreateErrorContext(result);
            inputName = result;
        }
        var outputResult = EvaluateOutputFile(inputName, inputType);
        if (!outputResult.successful)
        {
            return CreateErrorContext(outputResult.result);
        }
        var inputSettings = new InputSettings(
            inputType,
            inputName,
            skipContentTypeCheck,
            _options.JsonFramework.IsDefined ? _options.JsonFramework.Value : JsonFramework.Default);
        var yamlFormatSettings = _formatSettingsFactory.Create(_options);
        var outputSettings = new OutputSettings(outputResult.result, yamlFormatSettings);
        return new ConversionContext(inputSettings, outputSettings);
    }

    private (bool sucessful, string result) EvaluateHttpEndpointInput()
    {
        var url = _options.Url.Value;
        if (!_urlHelper.CheckIfUrlCorrect(url))
        {
            return (false, $"Provided endpoint url value (`{url}`) is incorrect");
        }
        return (true, url);
    }

    private (bool successful, string result) EvaluateFileInput()
    {
        var inputFileName = _options.Input.Value;
        var isFileNameProvided = !string.IsNullOrEmpty(_options.Input.Value);
        if (!isFileNameProvided)
        {
            var candidates = GetInputFileCandidates();
            if (candidates.Length > 1)
            {
                var errorMessageBuilder = new StringBuilder();
                errorMessageBuilder.AppendLine("There are more then one json file in given directory:");
                errorMessageBuilder.AppendLine("  " + string.Join(Environment.NewLine + "  ", candidates));
                errorMessageBuilder.AppendLine($"Choose only one. E.g.: `json2yaml -i:{candidates[0]}`.");
                return (false, errorMessageBuilder.ToString());
            }
            else if (candidates.Length == 0)
            {
                return (false, "Current directory doesn't contain json file. Please provide path to input.");
            }
            else
            {
                inputFileName = candidates[0];
            }
        }

        if (isFileNameProvided && !_ioHandler.IsSameExtension(inputFileName, ".json")) //needs to check only for filename value from options.
        {
            return (false, "Input file must be with json extension.");
        }
        var size = _options.MaxSize.IsDefined ? _options.MaxSize.Value : (int?)null;
        var (successful, result) = CheckFileSize(size, inputFileName);
        if (!successful)
        {
            return (successful, result);
        }
        return (true, inputFileName);
    }

    private string[] GetInputFileCandidates() =>
        _ioHandler.FindFiles(Directory.GetCurrentDirectory(), "*.json");

    private (bool successful, string result) CheckFileSize(int? size, string inputPath)
    {
        //TODO: start from here. Maybe leave parsing to int for mcmaster lib. And accept int here?
        var maxSize = size ?? _maxSizeMbDefault;
        if (_ioHandler.GetFileSize(inputPath) > maxSize)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"Input file size is larger then {maxSize} MB. Skipped.");
            builder.AppendLine($"Use --max-size option to override {(maxSize == _maxSizeMbDefault ? "default" : "given")} size limit.");
            return (successful: false, builder.ToString());
        }
        return (successful: true, string.Empty);
    }

    private (bool successful, string result) EvaluateOutputFile(string inputName, InputType inputType)
    {
        var writeToConsole = _options.ConsoleOutput.IsDefined;
        if (writeToConsole)
        {
            return (true, null); // will be written to console. This is not actual.
        }
        var outputInfo = _options.Output;
        if (outputInfo.IsDefined && outputInfo.Value is not null)
        {
            return (true, outputInfo.Value);
        }

        return inputType switch
        {
            InputType.File => (true, inputName.Replace(".json", ".yaml")),
            InputType.Http => (true, _urlHelper.ExtractFileName(inputName)),
            InputType.Console => (false, "When no input options (--url or --input) is provided either --output option with filename or --console-output flag is required."),
            _ => throw new ArgumentException($"{inputType} is not expected here."),
        };
    }

    private static ConversionContext CreateErrorContext(string message, bool shouldBeOnlyMessage = false)
    {
        var context = new ConversionContext();
        context.SetError(message, shouldBeOnlyMessage);
        return context;
    }

    private static ConversionContext CreateInfoContext(string message, bool shouldBeOnlyMessage = false)
    {
        var context = new ConversionContext();
        context.SetInfo(message, shouldBeOnlyMessage);
        return context;
    }
}
