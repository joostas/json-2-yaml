namespace Json2Yaml.Exceptions
{

    /// <summary>
    /// This type of exception should be used whenever
    ///  custom exception is thrown in code.
    /// </summary>
    public class Json2YamlException : System.Exception
    {
        public Json2YamlException()
        {
        }

        public Json2YamlException(string message) : base(message) 
        {
        }

        public Json2YamlException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}