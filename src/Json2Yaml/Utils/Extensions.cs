using System.Collections.Generic;
using System.Linq;
using McMaster.Extensions.CommandLineUtils;

namespace Json2Yaml.Utils
{
    public static class Extensions
    {
        public static string GetValue(this List<CommandOption> options, string optionName)
        {
            if (options is null)
            {
                throw new System.ArgumentNullException(nameof(options));
            }
            if (string.IsNullOrWhiteSpace(optionName))
            {
                throw new System.ArgumentException("message", nameof(optionName));
            }
            var option = options.FirstOrDefault(o => o.ShortName == optionName ||
                o.LongName == optionName);
            return option?.Value();
        }

        public static CommandOption Find(this List<CommandOption> options, string optionName)
        {
            if (options is null)
            {
                throw new System.ArgumentNullException(nameof(options));
            }
            if (string.IsNullOrWhiteSpace(optionName))
            {
                throw new System.ArgumentException("message", nameof(optionName));
            }
            var option = options.FirstOrDefault(o => o.ShortName == optionName ||
                o.LongName == optionName);
            return option;
        }
    }
}