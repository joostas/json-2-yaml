using src.Json2Yaml.Utils;

namespace Json2Yaml.Utils
{
    /// <summary>
    /// Contains information which is used when converting json to yaml and providing conversion results.
    /// </summary>
    public class ConversionContext
    {
        /// <summary>
        /// Creates empty conversion context. No actions can be applied on it.
        /// </summary>
        public ConversionContext()
        {
        }

        public InputSettings InputSettings { get; private set; }

        public OutputSettings OutputSettings { get; private set; }

        /// <summary>
        /// Initialized conversion context and martks as ready for conversion.
        /// </summary>
        /// <param name="inputType"></param>
        /// <param name="inputFileName"></param>
        /// <param name="outputFileName"></param>
        public ConversionContext(InputSettings inputSettings, OutputSettings outputSettings)
        {
            PerformConversion = true;
            InputSettings = inputSettings;
            OutputSettings = outputSettings;
        }

        public bool PerformConversion { get; private set; }

        public string Message { get; private set; }

        public MessageType MessageType { get; private set; }

        public bool ShouldBeOnlyMessage { get; private set; }

        /// <summary>
        /// Add error to context. Also mark as conversion cannot be implemented.
        /// </summary>
        /// <param name="message"></param>
        public void SetError(string message, bool shouldBeOnlyMessage = false)
        {
            PerformConversion = false;
            Message = message;
            MessageType = MessageType.Error;
            ShouldBeOnlyMessage = shouldBeOnlyMessage;
        }

        /// <summary>
        /// Add info to context. Also mark as conversion cannot be implemented.
        /// </summary>
        /// <param name="message"></param>
        public void SetInfo(string message, bool shouldBeOnlyMessage = false)
        {
            PerformConversion = false;
            Message = message;
            MessageType = MessageType.Info;
            ShouldBeOnlyMessage = shouldBeOnlyMessage;
        }
    }
}