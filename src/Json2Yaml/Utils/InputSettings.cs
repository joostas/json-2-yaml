using Json2Yaml.Input;
using Json2Yaml.Json;

namespace src.Json2Yaml.Utils;

public class InputSettings
{

    public InputSettings(
        InputType inputType,
        string inputFileName,
        bool skipContentTypeCheck,
        JsonFramework jsonFramework)
    {
        InputType = inputType;
        InputFileName = inputFileName;
        SkipContentTypeCheck = skipContentTypeCheck;
        JsonSettings = new JsonParserSettings(jsonFramework);
    }

    public InputType InputType { get; private set; }

    public string InputFileName { get; private set; }

    public bool SkipContentTypeCheck { get; private set; }

    public JsonParserSettings JsonSettings {get; private set;}

}
