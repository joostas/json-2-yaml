using System;
using System.IO;
using System.Linq;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Utils
{
    public class UrlHelper : IUrlHelper
    {
        /// <summary>
        /// Checks if given string is correct url.
        /// </summary>
        /// <param name="url"></param>
        /// <returns>If url is correct.</returns>
        public bool CheckIfUrlCorrect(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
                return false;
            if (!url.Contains("://"))
            {
                url = "http://" + url;
            }
            return Uri.TryCreate(url, UriKind.Absolute, out Uri uriResult) && 
            (uriResult.Scheme == Uri.UriSchemeHttps || uriResult.Scheme == Uri.UriSchemeHttp);
        }

        public string ExtractFileName(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                throw new ArgumentException($"{nameof(url)} cannot be null or empty");
            }
            if (!Uri.TryCreate(url, UriKind.Absolute, out Uri uriResult))
            {
                throw new ArgumentException("Provided url is malformed");
            }
            if (uriResult.IsFile)
            {
                throw new ArgumentException($"Provided file path, not url ({url})");
            }
            var name = uriResult.Segments.Last();
            if (Path.HasExtension(name))
            {
                name = Path.ChangeExtension(name, "yaml");
            }
            else
            {
                name += ".yaml";
            }
            var pattern = Path.GetInvalidFileNameChars();
            var isNameCorrect = !name.Any(pattern.Contains);
            if (!isNameCorrect)
            {
                throw new Json2YamlException($"Could not extract correct file name from given url (got {name}). Please provide output file name with --output option");
            }
            return name;
        }
    }
}