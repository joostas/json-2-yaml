using System;
using Autofac;
using Autofac.Core;
using Boxed.Mapping;
using Json2Yaml.Cli;
using Json2Yaml.Input;
using Json2Yaml.Interfaces;
using Json2Yaml.Json;
using Json2Yaml.Output;
using Json2Yaml.Yaml;
using McMaster.Extensions.CommandLineUtils.Abstractions;

namespace Json2Yaml.Utils;

/// <summary>
/// Contains registration of all dependencies.
/// </summary>
public static class DependencyRegistrer
{
    /// <summary>
    /// Register dependencies and builds container
    /// </summary>
    /// <returns>Container</returns>
    public static IContainer ConfigureDependencies(IModule overrides)
    {
        var builder = new ContainerBuilder();
        builder.RegisterType<InputReaderFactory>().As<IInputReaderFactory>();
        builder.RegisterType<OutputWriterFactory>().As<IOutputWriterFactory>();
        builder.RegisterType<RemoteIOHandler>().As<IRemoteIOHandler>();
        builder.RegisterType<IOHandler>().As<IIOHandler>();
        builder.RegisterType<UrlHelper>().As<IUrlHelper>();
        builder.RegisterType<VersionProvider>().As<IVersionProvider>();
        builder.RegisterType<Json2YamlCli>().As<IJson2YamlCli>();
        builder.RegisterType<Json2YamlConverter>().As<IJson2YamlConverter>();
        builder.RegisterType<JsonParserFactory>().As<IJsonParserFactory>();
        builder.RegisterType<NewtonsoftJsonParser>().As<IJsonParser>();
        builder.RegisterType<YamlProducer>().As<IYamlProducer>();
        builder.RegisterType<YamlFormatSettingsFactory>().As<IYamlFormatSettingsFactory>();
        builder.RegisterType<HttpInputReader>();
        builder.RegisterType<FileInputReader>();
        builder.RegisterType<ConsoleInputReader>();
        builder.RegisterType<CliApplicationHelper>().As<ICliApplicationHelper>();
        builder.Register(c => HttpClientFactory.CreateHttpClient());
        builder.RegisterType<ConsoleOutputWriter>().WithParameter(new TypedParameter(typeof(bool), false))
            .AsSelf().As<IOutputWriter>();
        builder.RegisterType<FileOutputWriter>().WithParameter(new TypedParameter(typeof(bool), true))
            .AsSelf(); //this must not be registered as IOutputWriter. Because this type must be only created through IOutputWriterFactory
        builder.RegisterType<ConsoleRedirectionMarker>().As<IConsoleRedirectionMarker>();
        builder.RegisterType<CliInputParser>().As<ICliInputParser>();
        builder.RegisterType<Constants>();
        builder.RegisterType<CommandOptions>();
        builder.Register<Func<CommandOptions, ICliOptionsInterpreter>>(context => {
            var c = context.Resolve<IComponentContext>();
            return (options) => new CliOptionsInterpreter(
                options,
                c.Resolve<IUrlHelper>(),
                c.Resolve<IIOHandler>(),
                c.Resolve<Constants>(),
                c.Resolve<IVersionProvider>(),
                c.Resolve<IYamlFormatSettingsFactory>());
            });
        builder.RegisterType<CliInitializer>().As<ICliInitializer>();
        builder.RegisterType<ParsedCommandMapper>().As<IImmutableMapper<ParseResult, ParsedCommand>>();
        if (overrides is not null)
        {
            builder.RegisterModule(overrides);
        }
        return builder.Build();
    }
}
