using System;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Utils
{
    /// <summary>
    /// Handles IO operations.
    /// </summary>
    public class IOHandler : IIOHandler
    {
        private readonly IFileSystem _fileSystem;
        private readonly IConsoleRedirectionMarker _marker;

        public IOHandler(IConsoleRedirectionMarker marker) : this(new FileSystem(), marker)
        {
        }

        public IOHandler(IFileSystem fileSystem, IConsoleRedirectionMarker marker)
        {
            _fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
            _marker = marker ?? throw new ArgumentNullException(nameof(marker));
        }

        public async Task<string> ReadFileAsync(string path, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Empty path is not supported.", nameof(path));
            }
            return await HandleReadIOAsync(() => _fileSystem.File.ReadAllTextAsync(path, cancellationToken), $"json input file '{path}'");
        }

        public Task<Stream> ReadStreamAsync(string path, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException($"'{nameof(path)}' cannot be null or whitespace.", nameof(path));
            }

            return Task.FromResult<Stream>(File.Open(path, FileMode.Open));
        }

        public async Task WriteFileAsync(string path, string content, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Empty path is not supported.", nameof(path));
            }
            await HandleWriteIOAsync(() => _fileSystem.File.WriteAllTextAsync(path, content, cancellationToken), $"yaml output file '{path}'");
        }

        public async Task WriteFileAsync(string path, Stream content, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Empty path is not supported.", nameof(path));
            }
            using var fileStream = _fileSystem.File.Create(path);
            await content.CopyToAsync(fileStream);
        }

        public bool IsSameExtension(string path, string extension)
        {
            if (string.IsNullOrWhiteSpace(path))
                return false;
            string pathExt; 
            try
            {
                pathExt = _fileSystem.Path.GetExtension(path);
            }
            catch(ArgumentException)
            {
                return false;
            }
            return pathExt.Equals(extension, StringComparison.OrdinalIgnoreCase);
        }

        public string[] FindFiles(string path, string extension)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Empty path is not supported.", nameof(path));
            }

            if (extension is null)
            {
                throw new ArgumentNullException(nameof(extension));
            }

            try
            {
                var files = _fileSystem.Directory.GetFiles(path, $"*{extension}", SearchOption.TopDirectoryOnly);
                return files.Select(f => _fileSystem.Path.GetRelativePath(path, f)).ToArray();
            }
            catch(UnauthorizedAccessException uaEx)
            {
                throw new Json2YamlException(
                    "The caller does not have the required permission to list files.",
                    uaEx);
            }
            catch(ArgumentException argEx)
            {
                throw new Json2YamlException(
                    $"The specified path `{path}` contains incorrect value.",
                    argEx);
            }
            catch(PathTooLongException pathEx)
            {
                throw new Json2YamlException(
                    $"The specified path, file name, or both exceed the system-defined maximum length.",
                    pathEx);
            }
            catch(DirectoryNotFoundException dirEx)
            {
                throw new Json2YamlException(
                    $"The specified directory `{path}` is not found or is invalid.",
                    dirEx);
            }
        }

        /// <summary>
        /// Returns filesize in MB.
        /// </summary>
        /// <param name="path">path to file</param>
        /// <returns></returns>
        public long GetFileSize(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException(nameof(path));
            }
            try
            {
                var fib = _fileSystem.FileInfo.New(path);
                return fib.Length / 1024 / 1024;
            }
            catch(FileNotFoundException fnEx)
            {
                throw new Json2YamlException($"{path} file was not found.", fnEx);
            }
            catch(IOException ioEx)
            {
                throw new Json2YamlException($"An I/O error occurred while opening file {path}.", ioEx);
            }
        }

        /// <summary>
        /// Shows if stdin is redirected.
        /// </summary>
        public bool IsInputRedirected => _marker.IsInputRedirected;

        private async Task HandleWriteIOAsync(Func<Task> writeFunc, string errorTemplate) => 
            await HandleIOAsync(async () => {await writeFunc(); return string.Empty; }, errorTemplate, "write");

        private async Task<string> HandleReadIOAsync(Func<Task<string>> ioFunc, string errorTemplate) =>
            await HandleIOAsync(ioFunc, errorTemplate, "read");

        private async Task<string> HandleIOAsync(Func<Task<string>> ioFunc, string errorTemplate, string action)
        {
            string output;
            try
            {
                output = await ioFunc();
            }
            catch(PathTooLongException plEx)
            {
                throw new Json2YamlException($"Path of {errorTemplate} too long.", plEx);
            }
            catch(DirectoryNotFoundException dnfEx)
            {
                throw new Json2YamlException($"Path of {errorTemplate} is invalid.", dnfEx);
            }
            catch(FileNotFoundException fnEx)
            {
                throw new Json2YamlException($"{errorTemplate} was not found.", fnEx);
            }
            catch(IOException ioEx)
            {
                throw new Json2YamlException($"An I/O error occurred while opening {errorTemplate}.", ioEx);
            }
            catch(NotSupportedException nsEx)
            {
                throw new Json2YamlException($"{errorTemplate} file path is in an invalid format.", nsEx);
            }
            catch(SecurityException perEx)
            {
                throw new Json2YamlException($"Missing permissions to {action} {errorTemplate}.", perEx);
            }
            catch(UnauthorizedAccessException unauthEx)
            {
                throw new Json2YamlException($"Access denied {action} {errorTemplate}.", unauthEx);
            }
            return output;
        }
    }
}