using System.Net.Http;

namespace Json2Yaml.Utils
{
    public class HttpClientFactory
    {
        public static HttpClient CreateHttpClient()
        {
            var httpClientHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
            };
            return new HttpClient(httpClientHandler);
        }
    }
}