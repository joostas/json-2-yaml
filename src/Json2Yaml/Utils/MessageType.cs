namespace Json2Yaml.Utils
{
    public enum MessageType
    {
        None = 0,
        Info = 1,
        Error = 2,
    }
}