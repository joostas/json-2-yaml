using System;
using System.Reflection;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Utils
{
    public class VersionProvider : IVersionProvider
    {
        private readonly Func<string> _getApplicationVersion;
        private readonly IOutputWriter _outputWriter;

        public VersionProvider(Func<string> getApplicationVersion, IOutputWriter outputWriter)
        {
            _getApplicationVersion = getApplicationVersion ?? throw new ArgumentNullException(nameof(getApplicationVersion));
            _outputWriter = outputWriter ?? throw new ArgumentNullException(nameof(outputWriter));
        }

        public VersionProvider(IOutputWriter outputWriter) : this(GetVersion, outputWriter)
        {
        }

        public string ReadProjectVersion()
        {
            try
            {
                var version = _getApplicationVersion();
                if (string.IsNullOrWhiteSpace(version))
                {
                    return "-";
                }
                return version;
            }
            catch
            {
                _outputWriter.WriteInfo("Warning: failed to read tool version.");
                return "-";
            }
        }

        private static string GetVersion()
        {
            var assembly = Assembly.GetExecutingAssembly();
            //InformationalVersion contains not only digits, but also suffix with literals.
            string informationVersion = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            
            //From .NET 8 this attribute also includes commit hash. 
            //https://learn.microsoft.com/en-us/dotnet/api/system.reflection.assemblyinformationalversionattribute?view=net-9.0#remarks
            //Need to remove that hash.
            return informationVersion.Split("+")[0];
        }
    }
}