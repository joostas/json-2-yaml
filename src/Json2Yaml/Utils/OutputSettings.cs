using Json2Yaml.Interfaces;

namespace src.Json2Yaml.Utils
{
    public class OutputSettings
    {
        public OutputSettings(string outputFileName, IYamlFormatSettings yamlFormatSettings)
        {
            OutputFileName = outputFileName;
            YamlFormatSettings = yamlFormatSettings;
        }

        public string OutputFileName { get; private set; }
        public IYamlFormatSettings YamlFormatSettings { get; private set; }
    }
}
