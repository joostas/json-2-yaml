using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Utils;

/// <summary>
/// Handles remote io related operation.
/// Fetching remote resorces using HTTP.
/// </summary>
public class RemoteIOHandler : IRemoteIOHandler
{
    private readonly HttpClient _httpClient;
    public RemoteIOHandler(HttpClient httpClient, IVersionProvider versionProvider)
    {
        if (versionProvider == null)
        {
            throw new ArgumentNullException(nameof(versionProvider));
        }

        _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        _httpClient.DefaultRequestHeaders.Accept
            .Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
        _httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue("json2yaml", versionProvider.ReadProjectVersion()));
    }

    /// <summary>
    /// Fetches json using HTTPClient from given url.
    /// </summary>
    /// <param name="url">Address of endpoint to fetch from.</param>
    /// <param name="skipContentTypeCheck">Marks if check of response Content-Type header can be skipped</param>
    /// <returns>Fetched json</returns>
    /// <exception cref="Json2YamlException"></exception>
    public async Task<string> FetchJsonAsync(string url, bool skipContentTypeCheck, CancellationToken cancellationToken = default)
    {
        if (url is null)
        {
            throw new ArgumentNullException(nameof(url));
        }

        try
        {
            string content;
            using (HttpResponseMessage response = await _httpClient.GetAsync(url, HttpCompletionOption.ResponseHeadersRead))
            {
                var contentType = response.Content.Headers.ContentType?.MediaType;
                if (!IsContentTypeAllowed(contentType, skipContentTypeCheck))
                {
                    throw new Json2YamlException($"Http response Contetnt type must be json, {contentType} is not supported. Tip: provide flag --skip-content-type-check to ignore this error.");
                }
                content = await response.Content.ReadAsStringAsync();
            }
            return content;
        }
        catch (Exception ex) when (!(ex is Json2YamlException))  //TODO: improve error handling.
        {
            throw new Json2YamlException($"Failed to fetch resource from `{url}`", ex);
        }
    }

    public async Task<Stream> FetchStreamAsync(string url, bool skipContentTypeCheck, CancellationToken cancellationToken)
    {
        ArgumentNullException.ThrowIfNull(nameof(url));
        try
        {
            HttpResponseMessage response = await _httpClient.GetAsync(url, HttpCompletionOption.ResponseHeadersRead);
            var contentType = response.Content.Headers.ContentType?.MediaType;
            if (!IsContentTypeAllowed(contentType, skipContentTypeCheck))
            {
                throw new Json2YamlException($"Http response Contetnt type must be json, {contentType} is not supported. Tip: provide flag --skip-content-type-check to ignore this error.");
            }
            return await response.Content.ReadAsStreamAsync(cancellationToken);
        }
        catch (Exception ex) when (!(ex is Json2YamlException))  //TODO: improve error handling.
        {
            throw new Json2YamlException($"Failed to fetch resource from `{url}`", ex);
        }
    }

    public static bool IsContentTypeAllowed(string contentType, bool skipContentTypeCheck) =>
        contentType is null || skipContentTypeCheck || contentType.ToLower().Contains("json");

}