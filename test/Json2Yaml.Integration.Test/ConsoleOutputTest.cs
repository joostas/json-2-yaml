using System.Threading.Tasks;
using Json2Yaml.Integration.Test.Helpers;
using Xunit;
using static Json2Yaml.Integration.Test.Helpers.AssertHelpers;

namespace Json2Yaml.Integration.Test
{
    [Collection("Integration")]
    public class ConsoleOutputTest
    {
        private readonly DirOptions _options = new DirOptions();
        public ConsoleOutputTest()
        {
        }

        [Theory]
        [InlineData("-i", "-c")]
        [InlineData("--input", "--console-output")]
        [InlineData("--input", "-c")]
        [InlineData("-i", "--console-output")]
        //json2yaml -i -c
        public async Task SimplestInputTestWithConsoleNoOutputFile(string inputOption, string consoleOption)
        {
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync(inputOption, consoleOption);
                Assert.Equal(0, status);
                AssertFileNotExist("test.yaml");
            }
        }

        [Theory]
        [InlineData("-i", "-c")]
        [InlineData("--input", "--console-output")]
        [InlineData("--input", "-c")]
        [InlineData("-i", "--console-output")]
        //json2yaml -i -c
        public async Task SimplestInputTestWithConsoleNoAdditionalOutput(string inputOption, string consoleOption)
        {
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync(inputOption, consoleOption);
                Assert.Equal(0, status);
                Assert.False(output.ContainsInfo("json2yaml tool run"),
                 "When outputing to console, no additional info should be printed here.");
            }
        }

        [Theory]
        [InlineData("-i", "-c")]
        [InlineData("--input", "--console-output")]
        [InlineData("--input", "-c")]
        [InlineData("-i", "--console-output")]
        //json2yaml -i -c
        public async Task SimplestInputTestWithConsole_ConsoleContainsOnlyConversionResult(string inputOption, string consoleOption)
        {
            _options.AddJsonFile("test.json");
            using(var dirPreparator = new DirPreparator(_options))
            {
                var expectedContent = dirPreparator.GetFileContent("sample.yaml", fromSamples: true);
                var result = await Json2YamlExeAsync(inputOption, consoleOption);
                Assert.Equal(0, result.status);
                var output = result.output.OutputLines.ToString();
                Assert.Equal(expectedContent, output);
            }
        }

        [Fact]
        //json2yaml -i -o:output.yaml -c
        public async Task OutputToConcoleIgnoreOutputOption()
        {
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var (status, _) = await Json2YamlExeAsync("-i", "-o:output.yaml", "-c");
                Assert.Equal(0, status);
                AssertFileNotExist("output.yaml");
            }
        }

        [Trait("Category", "Skip-CI")]
        [Fact]
        public async Task OutputToConsoleDoesntRequireOutputForUrlOption()
        {
            var (status, _) = await Json2YamlExeAsync($"-u:{Config.WorkingJsonEndpoint}", "-c");
            Assert.Equal(0, status);
        }
    }
}