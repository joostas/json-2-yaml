using Autofac;
using Json2Yaml.Integration.Test.Helpers;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Integration.Test
{
    public class IntegrationModule : Module
    {
        private readonly RunOptions _options;
        public IntegrationModule(RunOptions options)
        {
            _options = options ?? new RunOptions();
        }

        private TestOutputWriter _outputWriter;

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register<IOutputWriter>((c, p) =>
            {
                IComponentContext ctx = c.Resolve<IComponentContext>();
                var iohandler = ctx.Resolve<IIOHandler>();
                _outputWriter = new TestOutputWriter(iohandler);
                return _outputWriter;
            }).SingleInstance();
            builder.RegisterType<TestOutputWriterFactory>().As<IOutputWriterFactory>();
            builder.RegisterType<ManualConsoleRedirectionMarker>().As<IConsoleRedirectionMarker>()
                .WithParameter(new TypedParameter(typeof(bool), _options.MarkStdInAsRedirected));
        }

        public TestOutputWriter OutputWriter { get => _outputWriter; }
    }
}