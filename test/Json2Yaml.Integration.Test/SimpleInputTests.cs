using System;
using System.Collections.Generic;
using Json2Yaml.Integration.Test.Helpers;
using Json2Yaml.Yaml;
using Xunit;
using static Json2Yaml.Integration.Test.Helpers.AssertHelpers;

namespace Json2Yaml.Integration.Test;

//Doesn't use DirPreparator. Therefore can run parallel.
public class SimpleInputTests
{
    private readonly RunOptions _runOptions = new(){MarkStdInAsRedirected = true};

    public static IEnumerable<object[]> JsonParserOptions =>
        [
            ["--json-framework:system"],
            ["--json-framework:newtonsoft"],
        ];

    [Theory]
    [MemberData(nameof(JsonParserOptions))]
    public void ConvertSimpleInteger(string jsonOption)
    {
        StdInTestEmulator.Run("5",
        async () => {
            (int status, TestOutputWriter output) = await Json2YamlExeAsync(_runOptions, "-c", jsonOption);
            Assert.Equal(0, status);
            var result = output.OutputLines.ToString();
            Assert.Equal(AppendNeLine("5"), result);
        });
    }

    [Theory]
    [MemberData(nameof(JsonParserOptions))]
    public void ConvertSimpleNumber(string jsonOption)
    {
        StdInTestEmulator.Run("5.28",
        async () => {
            (int status, TestOutputWriter output) = await Json2YamlExeAsync(_runOptions, "-c", jsonOption);
            Assert.Equal(0, status);
            var result = output.OutputLines.ToString();
            Assert.Equal(AppendNeLine("5.28"), result);
        });
    }

    [Theory]
    [MemberData(nameof(JsonParserOptions))]
    public void ConvertSimpleString(string jsonOption)
    {
        StdInTestEmulator.Run("\"some text\"",
        async () => {
            (int status, TestOutputWriter output) = await Json2YamlExeAsync(_runOptions, "-c", jsonOption);
            Assert.Equal(0, status);
            var result = output.OutputLines.ToString();
            Assert.Equal(AppendNeLine("some text"), result);
        });
    }

    [Theory]
    [MemberData(nameof(JsonParserOptions))]
    //Actually this test shows issue in YamlDotNet lib.
    //Ideally "null" should be converted to "null" (string to string), but not (string to null)
    //Related Yaml.Net issue: https://github.com/aaubry/YamlDotNet/issues/387
    public void ConvertNullValue(string jsonOption)
    {
        StdInTestEmulator.Run("\"null\"",
        async () => {
            (int status, TestOutputWriter output) = await Json2YamlExeAsync(_runOptions, "-c", jsonOption);
            Assert.Equal(0, status);
            var result = output.OutputLines.ToString();
            Assert.Equal(AppendNeLine("null"), result);
        });
    }

    [Theory]
    [InlineData("true")]
    [InlineData("false")]
    //Actually this test shows issue in YamlDotNet lib.
    //Ideally "true" should be converted to "true" (string to string), but not (string to bool)
    //Related Yaml.Net issue: https://github.com/aaubry/YamlDotNet/issues/387
    public void ConvertBooleanValue(string boolString)
    {
        StdInTestEmulator.Run($"\"{boolString}\"",
        async () => {
            (int status, TestOutputWriter output) = await Json2YamlExeAsync(_runOptions, "-c");
            Assert.Equal(0, status);
            var result = output.OutputLines.ToString();
            Assert.Equal(AppendNeLine($"{boolString}"), result);
        });
    }

    [Theory]
    [MemberData(nameof(JsonParserOptions))]
    public void NullsConversionToDefaultnull(string jsonOption)
    {
        StdInTestEmulator.Run($"[null, null]",
        async () => {
            (int status, TestOutputWriter output) = await Json2YamlExeAsync(_runOptions, "-c", jsonOption);
            Assert.Equal(0, status);
            var result = output.OutputLines.ToString();
            //Expected:
            // - null
            // - null
            string nullValue = NullFormat.Default;
            Assert.Equal(AppendNeLine($"- {nullValue}{Environment.NewLine}- {nullValue}"), result);
        });
    }

    [Theory]
    [MemberData(nameof(JsonParserOptions))]
    public void ArrayWithSomeNullsConversion(string jsonOption)
    {
        StdInTestEmulator.Run("[null, 5, 3]",
            async () => {
                (int status, TestOutputWriter output) = await Json2YamlExeAsync(_runOptions, "-c", jsonOption);
                var result = output.OutputLines.ToString();
                Assert.Equal(0, status);
                string nullValue = NullFormat.Default;
                Assert.Equal($"- {nullValue}{Environment.NewLine}- 5{Environment.NewLine}- 3{Environment.NewLine}", result);
            });
    }

    private string AppendNeLine(string input) => input + Environment.NewLine;
}