using Xunit;
using Json2Yaml.Integration.Test.Helpers;
using static Json2Yaml.Integration.Test.Helpers.AssertHelpers;
using System.Linq;
using System.Threading.Tasks;

namespace Json2Yaml.Integration.Test
{
    [Collection("Integration")]
    public class IntegrationTests
    {
        private readonly DirOptions _options = new DirOptions();

        public IntegrationTests()
        {
            //_autofacMocks = new Module
        }

        [Fact]
        //(Skip="integration")]
        public async Task EmptyInputTestAsync()
        {
            var (status, _) = await Json2YamlExeAsync();
            Assert.Equal(0, status);
        }

        [Theory]
        [InlineData("-i")]
        [InlineData("--input")]
        public async Task SimplestInputTestAsync(string option)
        {
            _options.AddFile(1);
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync(option);
                Assert.Equal(0, status);
                AssertFileExists("test.yaml");
            }
        }

        [Theory]
        [InlineData("-i", "-o")]
        [InlineData("--input", "--output")]
        [InlineData("--input", "-o")]
        [InlineData("-i", "--output")]
        //json2yaml -i -o (file exists)
        public async Task SimpleInputOutputTestAsync(params string[] options)
        {
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync(options);
                Assert.Equal(0, status);
                AssertFileExists("test.yaml");
            }
        }

        [Theory]
        [InlineData("-i")]
        [InlineData("--input")]
        public async Task OnlyInputOptionTestAsync(string option)
        {
            _options.AddFile(1);
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync($"{option}:test.json");
                Assert.Equal(0, status);
                AssertFileExists("test.yaml");
            }
        }

        [Theory]
        [InlineData("-i", "-o")]
        [InlineData("-i", "--output")]
        [InlineData("--input", "-o")]
        [InlineData("--input", "--output")]
        //json2yaml -i:test.json -o:yaml.yaml (file exists)
        public async Task CorrectInputAndOutputTestAsync(string input, string output)
        {
            _options.AddFile(1);
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var result = await Json2YamlExeAsync($"{input}:test.json", $"{output}:yaml.yaml");
                Assert.Equal(0, result.status);
                AssertFileExists("yaml.yaml");
            }
        }

        [Theory]
        [InlineData("-i")]
        [InlineData("--input")]
        //json2yaml -i (several files exists)
        public async Task EmptyInputOptionAndSeveralJsonFilesTestAsync(string inputOption)
        {
            _options.AddJsonFile(3);
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync(inputOption);
                Assert.Equal(1, status);
                AssertContains("There are more then one json file in given directory:", output);
                AssertContains("Choose only one.", output);
            }
        }

        [Theory(Skip="Big input file")]
        [InlineData("--max-size")]
        [InlineData("-m")]
        public async Task BigJsonConversionTestWithCorrectFlagAsync(string maxsize)
        {
            _options.AddBigJson("large.json");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync("-i", $"{maxsize}:500");
                Assert.Equal(0, status);
                AssertFileExists("large.yaml");
            }
        }

        [Fact]
        //json2yaml -i:big.json
        public async Task BigJsonConversionTestNoMaxSizeOptionAsync()
        {
            _options.AddBigJson("large.json");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync("-i");
                Assert.Equal(1, status);
                AssertFileNotExist("large.yaml");
                AssertContains("Input file size is larger then 20 MB. Skipped.", output);
                AssertContains("Use --max-size option to override default size limit.", output);
            }
        }

        [Theory]
        [InlineData("--max-size")]
        [InlineData("-m")]
        //json2yaml -i:big.json --max-size:5
        public async Task BigJsonConversionTest_ToLittleSizeOptionAsync(string maxsizeOption)
        {
            var sizeNumber = 5;
            _options.AddBigJson("large.json");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync("-i", $"{maxsizeOption}:{sizeNumber}");
                Assert.Equal(1, status);
                AssertContains($"Input file size is larger then {sizeNumber} MB. Skipped.", output);
                AssertContains("Use --max-size option to override given size limit.", output);
                AssertFileNotExist("large.yaml");
            }
        }

        [Fact]
        public async Task DirContainesFileWithSameNameAsOutputAsync()
        {
            _options.AddFile("output.yaml");
            _options.AddJsonFile("input.json");
            using(new DirPreparator(_options))
            {
                var (status, _) = await Json2YamlExeAsync("-i:input.json", "-o:output.yaml");
                Assert.Equal(0, status);
                AssertFileExists("output.yaml"); //TODO make sure that content differs.
            }
        }

        [Fact]
        public async Task FileNameContainsSpaceTestAsync()
        {
            _options.AddJsonFile("input with space.json");
            using(new DirPreparator(_options))
            {
                var (status, _) = await Json2YamlExeAsync("-i:input with space.json", "-o:output with space.yaml");
                Assert.Equal(0, status);
                AssertFileExists("output with space.yaml"); //TODO make sure that content differs.
            }
        }

        [Fact]
        public async Task NonExistingInputFileTestAsync()
        {
            using(new DirPreparator())
            {
                var (status, output) = await Json2YamlExeAsync("-i:doesntexist.json");
                Assert.Equal(1, status);
                AssertContains("doesntexist.json file was not found.", output);
            }
        }

        [Theory]
        [InlineData("-o")]
        [InlineData("--output")]
        //json2yaml -o should return 1 and expected error message
        public async Task EmptyOutputOptionErrorTestAsync(string outputOption)
        {
            var (status, output) = await Json2YamlExeAsync(outputOption);
            Assert.Equal(1, status);
            AssertErrorContains("When no input options (--url or --input) is provided either --output option with filename or --console-output flag is required.", output);
        }

        [Fact]
        //json2yaml
        public async Task RunWithNoOptionsShowsHelpInfoAsync()
        {
            var (status, _) = await Json2YamlExeAsync();
            //we cannot check that output contains text from help,
            // because it doesn't go to mocked info pipeline.
            Assert.Equal(0, status);
        }

        [Fact]
        //json2yaml --version
        public async Task VersionOptionTestAsync()
        {
            var (status, output) = await Json2YamlExeAsync("--version");
            Assert.Equal(0, status);
            var version = output.MessageLines.ToString();
            var versionNumbersPart = version.Split('-').First();
            var versionNumbers = versionNumbersPart.Split('.');
            Assert.True(versionNumbers.All(o => int.TryParse(o, out _)), $"Version must contain numbers d.d.d. Now {version}");
        }

        [Theory(/*Skip="Try to find problem"*/)]
        [InlineData("-u")]
        [InlineData("--input,-o")]
        [InlineData("-m:20")]
        ///json2yaml --option --version should ignore version option.
        public async Task IgnoreVersionOptionIfThereAreMoreOptionsAsync(string otherOptions)
        {
            var options = otherOptions.Split(',').ToList();
            options.Add("--version");
            var (_, output) = await Json2YamlExeAsync(options.ToArray());
            Assert.False(IsVersionString(output.MessageLines.ToString()), "Should not show only version");
        }

        [Trait("Category", "Skip-CI")] //TODO - not clear why it fails on gitlab...
        [Theory]
        [InlineData("-u:url,-o:output.yaml")]
        ///json2yaml --option --version should ignore version option.
        public async Task IgnoreVersionOptionIfThereAreMoreOptions_LocalOnlyAsync(string otherOptions)
        {
            var options = otherOptions.Split(',').ToList();
            options.Add("--version");
            var (_, output) = await Json2YamlExeAsync(options.ToArray());
            Assert.False(IsVersionString(output.MessageLines.ToString()), "Should not show only version");
        }

        [Fact]
        public async Task SkipContentCheckDoesNotAffectFileInput()
        {
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var result = await Json2YamlExeAsync($"--input:test.json", $"--output:yaml.yaml", "--skip-content-type-check");
                Assert.Equal(0, result.status);
                AssertFileExists("yaml.yaml");
            }
        }

        private bool IsVersionString(string line)
        {
            var versionNumber = line.Split('-').First();
            var versionNumbers = line.Split('.');
            return versionNumbers.All(o => int.TryParse(o, out _));
        }
    }
}