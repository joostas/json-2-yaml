using System.Threading.Tasks;
using Json2Yaml.Integration.Test.Helpers;
using Xunit;
using static Json2Yaml.Integration.Test.Helpers.AssertHelpers;

namespace Json2Yaml.Integration.Test
{
    [Collection("Integration")]
    public class ConsoleInputTests
    {
        private readonly DirOptions _options = new DirOptions();
        private readonly RunOptions _runOptions = new RunOptions(){MarkStdInAsRedirected = true};
        
        [Theory]
        [InlineData("-o")]
        [InlineData("--output")]
        public void ReadFromConsoleTest(string outputOption)
        {
            StdInTestEmulator.RunUsingInputFile("test.json", (dirPreparator) =>
                {
                    var (status, output) = Json2YamlExeAsync(_runOptions, $"{outputOption}:tst.yaml").Result; //Result is needed to be able to check AssertFileExists 
                    Assert.True(0 == status, output.ExceptionLines.ToString());
                    AssertFileExists("tst.yaml");
                });
        }

        [Fact]
        public void ReadFromConsoleConvertedContentResult()
        {
            StdInTestEmulator.RunUsingInputFile("test.json",
             (dirPreparator) => {
                 var expected = dirPreparator.GetFileContent("sample.yaml", fromSamples:true);
                 (int status, TestOutputWriter output) = Json2YamlExeAsync(_runOptions, "-c").Result;
                 var content = output.OutputLines.ToString();
                 Assert.Equal(0, status);
                 Assert.Equal(expected, content);
             });
        }

        [Theory]
        [InlineData("-i")]
        [InlineData("--input")]
        public void IfInputOptionIsPresentedIgnoreConsole(string inputOption)
        {
            _options.AddJsonFileDifferent("other.json");
            StdInTestEmulator.RunUsingInputFile("test.json",
            (dirPreparator) => {
                var expectedToBeDifferent = dirPreparator.GetFileContent("sample.yaml", fromSamples:true);
                (int status, TestOutputWriter output) = Json2YamlExeAsync(_runOptions, $"{inputOption}:other.json").Result;
                var content = output.OutputLines.ToString();
                Assert.Equal(0, status);
                AssertFileExists("other.yaml");
                Assert.NotEqual(expectedToBeDifferent, content);
            },
            _options);
        }

        [Fact]
        //json2yaml -o:output.yaml without redirected input must return 1 with error message
        public async Task NoInputFromStdInTest()
        {
            (int status, TestOutputWriter output) = await Json2YamlExeAsync("-o:output.yaml");
            Assert.True(status == 1);
            Assert.True(output.ContainsError("No input from stdin"));
        }

        [Theory]
        [InlineData("-o")]
        [InlineData("--output")]
        //input.json | json2yaml -o should return 1 and expected error message
        public void ConsoleInputEmptyOutputOptionErrorTest(string outputOption)
        {
            StdInTestEmulator.RunUsingInputFile("test.json",
            async (DirPreparator) => {
                (int status, TestOutputWriter output) = await Json2YamlExeAsync(_runOptions, $"{outputOption}");
                var error = output.ExceptionLines.ToString();
                Assert.Equal(1, status);
                AssertErrorContains("When no input options (--url or --input) is provided either --output option with filename or --console-output flag is required.", output);
            });
        }

        [Fact]
        public void SkipContentCheckDoesNotAffectConsoleInput()
        {
            StdInTestEmulator.RunUsingInputFile("test.json",
            (DirPreparator) => {
                (int status, TestOutputWriter output) = Json2YamlExeAsync(_runOptions, "--skip-content-type-check").Result;
                var error = output.ExceptionLines.ToString();
                Assert.Equal(1, status);
                Assert.Contains("When no input options (--url or --input) is provided either --output option with filename or --console-output flag is required.", error);
            });
        }

        [Fact]
        //input.json | json2yaml
        public void RunWithRedirectedInputAndNoOptionsDoesntShowHelpInfo()
        {
            StdInTestEmulator.RunUsingInputFile("test.json",
            async (DirPreparator) => {
                (int status, TestOutputWriter output) = await Json2YamlExeAsync(_runOptions);
                var error = output.ExceptionLines.ToString();
                Assert.Equal(1, status);
                Assert.Contains("Missing output information. Provide it using --output option or --console-output flag.", error);
            });
        }
    }
}