using System.Threading.Tasks;
using Json2Yaml.Integration.Test.Helpers;
using Xunit;
using static Json2Yaml.Integration.Test.Helpers.AssertHelpers;

namespace Json2Yaml.Integration.Test
{
    [Collection("Integration")]
    public class VerboseModeTests
    {
        private readonly DirOptions _options = new DirOptions();
        
        [Theory]
        [InlineData("--verbose")]
        [InlineData("-v")]
        //json2yaml -v -i
        public async Task VersboseShouldReturnDetails(string verboseFlag)
        {
            _options.AddIncorrectJsonFile();
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync(verboseFlag, "-i");
                Assert.Equal(1, status);
                AssertContains(" at ", output);
            }
        }

        [Theory]
        [InlineData("--verbose")]
        [InlineData("-v")]
        //json2yaml -v -i -c
        public async Task ConsoleOutputVerboseShouldReturnDetails(string verboseFlag)
        {
            _options.AddIncorrectJsonFile();
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync(verboseFlag, "-i", "-c");
                Assert.Equal(1, status);
                AssertContains(" at ", output);
            }
        }

        [Fact]
        public async Task MissingVerboseFlagShouldPreventErrorDetails()
        {
            _options.AddIncorrectJsonFile();
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync("-i");
                Assert.Equal(1, status);
                AssertDoesntContain(" at ", output);
            }
        }

        [Theory]
        [InlineData("--verbose")]
        [InlineData("-v")]
        //json2yaml -v
        public async Task OnlyVerboseFlagShowsHelp(string verboseFlag)
        {
            var (status, output) = await Json2YamlExeAsync(verboseFlag);
            Assert.Equal(0, status);
            //Currently is not possible to access Help output. If output is empty we can assume that help was printed.
            Assert.Empty(output.MessageLines.ToString());
            Assert.Empty(output.OutputLines.ToString());
        }

        [Theory]
        [InlineData("--verbose", "--help")]
        [InlineData("--verbose", "-h")]
        [InlineData("-v", "-h")]
        [InlineData("-v", "--help")]
        //json2yaml -v -h
        public async Task VerboseAndHelpFlagsShowsHelp(string verboseFlag, string helpFlag)
        {
            var (status, output) = await Json2YamlExeAsync(verboseFlag, helpFlag);
            Assert.Equal(0, status);
            //Currently is not possible to access Help output. If output is empty we can assume that help was printed.
            Assert.Empty(output.MessageLines.ToString());
            Assert.Empty(output.OutputLines.ToString());
        }
    }
}