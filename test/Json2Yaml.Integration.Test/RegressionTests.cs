using System.Threading.Tasks;
using Xunit;
using static Json2Yaml.Integration.Test.Helpers.AssertHelpers;

namespace Json2Yaml.Integration.Test
{
    [Collection("Integration")]
    public class RegressionTests
    {
        [Theory]
        [InlineData("-m")]
        [InlineData("--max-size")]
        //json2yaml -m should complain about incorrect -m value
        public async Task MaxSizeValueCannotBeEmpty(string maxSizeOption)
        {
            var (status, output) = await Json2YamlExeAsync(maxSizeOption);
            Assert.Equal(1, status);
            Assert.True(
                output.ContainsError("Ups. Failed to parse input arguments. Option -m|--max-size cannot be empty."),
                $"Errors: {output}");
        }

        [Theory]
        [InlineData("-u")]
        [InlineData("--url")]
        //json2yaml -u should complain about missing -u option value
        public async Task UrlValueCannotBeEmpty(string urlOption)
        {
            var (status, output) = await Json2YamlExeAsync(urlOption);
            urlOption.TrimStart('-');
            Assert.Equal(1, status);
            Assert.True(
                output.ContainsError("Ups. Failed to parse input arguments. Option -u|--url cannot be empty."),
                $"Errors: {output}");
        }

        [Fact]
        //json2yaml -u -c
        public async Task UrlAndConsoleOptionsShouldNotRequireOutputFile()
        {
            var (status, output) = await Json2YamlExeAsync("-u", "-c");
            Assert.Equal(1, status);
            Assert.True(output.ContainsError("Ups. Failed to parse input arguments. Option -u|--url cannot be empty."));
        }

    }
}