using Json2Yaml.Interfaces;

namespace Json2Yaml.Integration.Test.Helpers;

/// <summary>
/// This helps to workaround issue in dotnet that after redirecting
/// console input from file using <c>Console.SetIn()</c> 
/// <c>Console.IsInputRedirected</c> still returns false.
/// This is needed to be able to run integration tests for console input cases.
/// </summary>
public class ManualConsoleRedirectionMarker : IConsoleRedirectionMarker
{
    private readonly bool _isRedirected;
    public ManualConsoleRedirectionMarker(bool isRedirected)
    {
        _isRedirected = isRedirected;
    }

    public bool IsInputRedirected => _isRedirected;

    /// <summary>
    /// For some reason when running integration tests it is not possible to change console input encoding.
    /// Could be related to using of Console.SetIn()...
    /// </summary>
    public bool CanUpdateEncoding => false;
}