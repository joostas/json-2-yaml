namespace Json2Yaml.Integration.Test.Helpers
{
    /// <summary>
    /// These options lets to control tool execiontion behaviour
    /// during integration testing.
    /// </summary>
    public class RunOptions
    {
        /// <summary>
        /// Marks if console is redirected.
        ///  This must be set to true after using Console.SetIn()
        /// </summary>
        public bool MarkStdInAsRedirected {get; set;}
    }
}