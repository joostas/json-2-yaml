using System.ComponentModel.DataAnnotations;
using System.Net.Mime;
using System;
using System.Collections.Generic;
using System.IO;

namespace Json2Yaml.Integration.Test.Helpers
{
    public class DirOptions
    {
        private string _dirPath;
        private bool _isInitialized;
        private readonly List<string> _jsonFiles = new List<string>();
        private readonly List<(string fileName, string content)> _jsonCustomFiles = new List<(string fileName, string content)>();
        private readonly List<string> _jsonFilesDifferent = new List<string>();
        private readonly List<string> _files = new List<string>();
        private string _incorrectJsonFile;

        private string _bigJsonFile;

        public void AddJsonFile(string name = null)
        {
            name = CreateFileName(name, ".json");
            _jsonFiles.Add(name);
        }

        public void AddJsonFileWithContent(string name, string content)
        {
            name = CreateFileName(name, ".json");
            _jsonCustomFiles.Add((name, content));
        }

        /// <summary>
        /// Allows to add json file which has different content compared to added through <c>AddJsonFile</c>
        /// </summary>
        /// <param name="name"></param>
        public void AddJsonFileDifferent(string name = null)
        {
            name = CreateFileName(name, ".json");
            _jsonFilesDifferent.Add(name);
        }

        public void AddBigJson(string name = null)
        {
            name = CreateFileName(name, ".json");
            _bigJsonFile = name;
        }

        public void AddJsonFile(int count)
        {
            for(var i = 0; i < count; i++)
                AddJsonFile();
        }

        public void AddIncorrectJsonFile(string name = null)
        {
            name = CreateFileName(name, ".json");
            _incorrectJsonFile = name;
        }

        public void AddFile(string name = null)
        {
            name = CreateFileName(name);
            _jsonFiles.Add(name);
        }

        public void AddFile(int count)
        {
            for(var i = 0; i < count; i++)
                AddFile();
        }

        private string CreateFileName(string name, string extension = "")
        {
            if (string.IsNullOrEmpty(name))
            {
                name = Path.GetRandomFileName() + extension;
            }
            else if (!name.ToLower().EndsWith(extension))
            {
                name += extension;
            }
            return name;
        }

        public void Initialize(string dir)
        {
            if (_isInitialized)
                throw new InvalidDataException("Current setup was already initialized. Cannot do it secound time");
            _dirPath = Path.GetFullPath(dir);
            _isInitialized = true;
            foreach(var file in _jsonFiles)
            {
                CopyFile("sample.json", file);
            }
            foreach(var file in _files)
            {
                CopyFile("text.txt", file);
            }
            foreach(var file in _jsonFilesDifferent)
            {
                CopyFile("sample2.json", file);
            }
            CreateCustomJsonFiles();
            if (!string.IsNullOrEmpty(_incorrectJsonFile))
            {
                CopyFile("incorrect.json", _incorrectJsonFile);
            }
            if(!string.IsNullOrEmpty(_bigJsonFile))
            {
                CopyFile("big.json", MakePath(_bigJsonFile));
            }
        }

        private void CreateCustomJsonFiles()
        {
            foreach(var (fileName, content) in _jsonCustomFiles)
            {
                var path = MakePath(fileName);
                File.WriteAllText(path, content);
            }
        }

        public string GetFileContent(string path, string basePath)
        {
            if (!string.IsNullOrEmpty(basePath))
            {
                path = Path.Combine(basePath, Config.SamplesDir, path);
            }
            return File.ReadAllText(path);
        }
        
        private string MakePath(string path) => Path.Combine(_dirPath, path);

        private void CopyFile(string from, string to)
        {
            if (string.IsNullOrWhiteSpace(from))
            {
                throw new System.ArgumentException(nameof(from));
            }

            if (string.IsNullOrWhiteSpace(to))
            {
                throw new System.ArgumentException(nameof(to));
            }

            File.Copy(Path.Combine(Config.SamplesDir, from), MakePath(to));
        }
    }
}