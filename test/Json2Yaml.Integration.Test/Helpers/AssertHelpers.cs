using System;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace Json2Yaml.Integration.Test.Helpers
{
    public static class AssertHelpers
    {
        public static void AssertFileExists(string fileName, string message = "")
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                message = $"File {fileName} does not exist";
            }
            Assert.True(File.Exists(fileName), message);
        }

        public static void AssertFileNotExist(string fileName, string message = "")
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                message = $"File {fileName} exists. But should not.";
            }
            Assert.False(File.Exists(fileName), message);
        }

        /// <summary>
        /// Calls dotnet json2yaml for integration tests.
        /// </summary>
        /// <param name="args">Arguments to be passed for running command to parse.</param>
        /// <returns>Program result code and captured output.</returns>
        public static Task<(int status, TestOutputWriter output)> Json2YamlExeAsync(params string[] args) => Json2YamlExeAsync(null, args);

        /// <summary>
        /// Calls dotnet json2yaml for integration tests.
        /// </summary>
        /// <param name="option">Allows to configure running command behaviour.</param>
        /// <param name="args">Arguments to be passed for running command to parse.</param>
        /// <returns>Program result code and captured output.</returns>
        public static async Task<(int status, TestOutputWriter output)> Json2YamlExeAsync(RunOptions options, params string[] args)
        {
            var module = new IntegrationModule(options);
            var status = await Program.StartAsync(args, module);
            return (status, module.OutputWriter);
        }

        public static void AssertContains(string expected, TestOutputWriter output)
        {
            Assert.True(output?.Contains(expected), $"Output doesn't contain text '{expected}' {output}");
        }

        public static void AssertConversionOutputContains(string expected, TestOutputWriter output) =>
            Assert.True(output?.ContainsOutput(expected),
                $"Conversion output doesn't contain text{Environment.NewLine}{expected}{Environment.NewLine}full output text:{Environment.NewLine}{output.OutputLines}");

        public static void AssertDoesntContain(string expected, TestOutputWriter output)
        {
            Assert.True(!output?.Contains(expected),
                $"Output must not contain text{Environment.NewLine}{expected}{Environment.NewLine}full output text:{Environment.NewLine}");
        }

        public static void AssertErrorContains(string expected, TestOutputWriter output) =>
            Assert.True(output?.ContainsError(expected),
                $"Error message doesn't contain text{Environment.NewLine}{expected}{Environment.NewLine}full output text:{Environment.NewLine}{output.ExceptionLines}");
    }
}