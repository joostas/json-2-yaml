using System;
using System.IO;
using System.Text;

namespace Json2Yaml.Integration.Test.Helpers
{
    /// <summary>
    /// Emulates that imput comes from stdin.
    /// </summary>
    public static class StdInTestEmulator
    {
        private static object _lock = new object();
        public static void RunUsingInputFile(
            string consoleInputFile,
            Action<DirPreparator> act,
            DirOptions options = null,
            Action prepare = null)
        {
            lock(_lock)
            {
                TextReader stdIn = Console.In;
                try
                {
                    if(!(prepare is null))
                    {
                        prepare();
                    }
                    if (options is null)
                    {
                        options = new DirOptions();
                    }
                    options.AddJsonFile(consoleInputFile);
                    using(var dirPreparator = new DirPreparator(options))
                    using(var reader = new StreamReader(consoleInputFile))
                    {
                        Console.SetIn(reader);
                        act(dirPreparator);
                    }
                }
                finally
                {
                    Console.SetIn(stdIn);
                }
            }
        }

        public static void Run(string content, Action act, Action prepare = null)
        {
            lock(_lock)
            {
                TextReader stdIn = Console.In;
                try
                {
                    if(!(prepare is null))
                    {
                        prepare();
                    }
                    using(var memoryStream = GenerateStreamFromString(content))
                    using(var reader = new StreamReader(memoryStream))
                    {
                        Console.SetIn(reader);
                        act();
                    }
                }
                finally
                {
                    Console.SetIn(stdIn);
                }
            }
        }

        private static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            using(var writer = new StreamWriter(stream, Encoding.UTF8, 512, leaveOpen: true))
            {
                writer.Write(s);
                writer.Flush();
            };
            stream.Position = 0;
            return stream;
        }
    }
}