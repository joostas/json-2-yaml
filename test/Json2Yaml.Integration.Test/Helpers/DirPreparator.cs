using System;
using System.IO;
using System.Threading;

namespace Json2Yaml.Integration.Test.Helpers
{
    public class DirPreparator : IDisposable
    {
        private readonly string _path;
        private readonly DirOptions _options;
        private readonly string _oldPath = Directory.GetCurrentDirectory();

        public DirPreparator() : this(Path.GetRandomFileName())
        {
        }
        public DirPreparator(string path)
        {
            _path = path;
            if (Directory.Exists(path))
            {
                throw new InvalidOperationException($"Directory {path} already exists");
            }
            Directory.CreateDirectory(path);
            //this don't allow to execute in parallel
            Directory.SetCurrentDirectory(path);
        }

        public DirPreparator(DirOptions options)
        {
            _path = Path.GetRandomFileName();
            _options = options;
            if (Directory.Exists(_path))
            {
                throw new InvalidOperationException($"Directory {_path} already exists");
            }
            Directory.CreateDirectory(_path);
            options.Initialize(_path);
            Directory.SetCurrentDirectory(_path);
        }

        public string GetFileContent(string path, bool fromSamples = false)
        {
            var options = _options ?? new DirOptions();
            return options.GetFileContent(path, fromSamples ? _oldPath : null);
        }
        
        public void Dispose()
        {
            Directory.SetCurrentDirectory(_oldPath);
            
            if (Directory.Exists(_path))
            {  
                try
                {
                    Directory.Delete(_path, true);
                }
                catch(IOException)
                {
                    //Lets wait a little bit in case of failure and try to delete again. It helps.
                    Thread.Sleep(200);
                    Directory.Delete(_path, true);
                }
            }
        }
    }
}