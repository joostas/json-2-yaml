using Autofac;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Integration.Test.Helpers
{
    public class TestOutputWriterFactory : IOutputWriterFactory
    {
        private readonly ILifetimeScope _scope;

        public TestOutputWriterFactory(ILifetimeScope scope)
        {
            _scope = scope;
        }

        public IOutputWriter Create(bool writeToConsole, bool verbose)
        {
            var outputWriter = _scope.Resolve<IOutputWriter>(new TypedParameter(typeof(bool), writeToConsole));
            var testOutputWriter = (TestOutputWriter)outputWriter;
            testOutputWriter.OutputToConsole = writeToConsole;
            testOutputWriter.Verbose = verbose;
            return outputWriter;
        }
    }
}