using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Integration.Test.Helpers
{
    /// <summary>
    /// Implementation for integration testing to be able to 
    /// capture and analyze program 'UI' output.
    /// </summary>
    public class TestOutputWriter : IOutputWriter
    {
        private readonly IIOHandler _iOHandler;
        public bool OutputToConsole { get; set; }
        public bool Verbose {get; set;}

        public TestOutputWriter(IIOHandler iOHandler)
        {
            ExceptionLines = new StringBuilder();
            MessageLines = new StringBuilder();
            OutputLines = new StringBuilder();
            _iOHandler = iOHandler ?? throw new ArgumentNullException(nameof(iOHandler));
        }

        public StringBuilder ExceptionLines { get; }
        public StringBuilder MessageLines { get; }

        public StringBuilder OutputLines { get; }

        public Exception Exception { get; private set; }

        public void WriteError(Exception ex)
        {
            if(Verbose)
            {
                ExceptionLines.AppendLine(ex.ToString());
            }
            else
            {
                ExceptionLines.AppendLine(ex.Message);
            }
            
            Exception = ex;
            Console.WriteLine(ex);
        }

        public void WriteError(string message)
        {
            ExceptionLines.AppendLine(message);
            Console.WriteLine(message);
        }

        public void WriteInfo(string message)
        {
            MessageLines.AppendLine(message);
            Console.WriteLine(message);
        }

        public async Task WriteOutputAsync(string output, string path = null, CancellationToken cancellationToken = default)
        {
            if (!(path is null) && !OutputToConsole)
            {
                await _iOHandler.WriteFileAsync(path, output, cancellationToken)/*.RunSynchronously()*/;
            }
            else
            {
                OutputLines.Append(output);
            }
        }

        public async Task WriteOutputAsync(Stream output, string path, CancellationToken token)
        {
            if (!(path is null) && !OutputToConsole)
            {
                await _iOHandler.WriteFileAsync(path, output, token)/*.RunSynchronously()*/;
            }
            else
            {
                using var reader = new StreamReader(output, leaveOpen: true);
                OutputLines.Append(await reader.ReadToEndAsync());
            }
        }

        public bool Contains(string term) => ContainsInfo(term) || ContainsError(term) || ContainsOutput(term);

        public bool ContainsInfo(string term) => MessageLines.ToString().Contains(term);

        public bool ContainsError(string term) => ExceptionLines.ToString().Contains(term);

        public bool ContainsOutput(string term) => OutputLines.ToString().Contains(term);

        public override string ToString()
        {
            return $@"{Environment.NewLine}Output:
            Info:
            {MessageLines.ToString()}
            Errors:
            {ExceptionLines.ToString()}";
        }
    }
}