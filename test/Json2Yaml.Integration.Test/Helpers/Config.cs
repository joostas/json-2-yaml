namespace Json2Yaml.Integration.Test.Helpers
{
    public static class Config
    {
        public static string SamplesDir => "../../../TestFiles/";

        public static string ExpectedFileNameFromWorkingJsonEndpoint = "dbaas.yaml";
        
        public static string WorkingJsonEndpoint => "http://rackerlabs.github.io/wadl2swagger/openstack/swagger/dbaas.json";
        
        public static string WrongMimeTypeJsonEndpoint => "https://raw.githubusercontent.com/smurakami/figure-scanner/e5dba206c1f529af2ac82d137f75a4b82daab9a2/figure_features_filename.json";
    }
}