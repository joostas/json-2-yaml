using System.Threading.Tasks;
using Json2Yaml.Integration.Test.Helpers;
using Xunit;
using static Json2Yaml.Integration.Test.Helpers.AssertHelpers;

namespace Json2Yaml.Integration.Test;

public class JsonParserTests
{
    private readonly DirOptions _options = new();

    [Theory]
    [InlineData("nested.json")]
    [InlineData("sample.json")]
    [InlineData("sample2.json")]
    public async Task SystemParsersProducesSameOutputNewtonsoft(string fileName)
    {
        _options.AddFile(fileName);
        using var _ = new DirPreparator(_options);
        var (statusOld, outputSystem) = await Json2YamlExeAsync("-i", "-c", "--json-framework:system");
        var (statusNew, outputNewtonsoft) = await Json2YamlExeAsync("-i", "-c", "--json-framework:newtonsoft");
        Assert.Equal(statusOld, statusNew);
        var yamlFromSystem = outputSystem.OutputLines.ToString();
        var yamlFromNewtonsoft = outputNewtonsoft.OutputLines.ToString();
        Assert.Equal(yamlFromSystem, yamlFromNewtonsoft);
    }
}