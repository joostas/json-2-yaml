using System;
using System.Threading.Tasks;
using Json2Yaml.Integration.Test.Helpers;
using Xunit;
using static Json2Yaml.Integration.Test.Helpers.AssertHelpers;

namespace Json2Yaml.Integration.Test
{
    public class YamlFormatTests
    {
        private readonly DirOptions _options = new DirOptions();
        
        [Fact]
        public async Task NullInputTestAsync()
        {
            _options.AddJsonFileWithContent("test.json", "[null]");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync("-i", "--format-null-as:null", "-c");
                Assert.Equal(0, status);
                AssertConversionOutputContains("null", output);
            }
        }

        [Theory]
        [InlineData("empty", "")]
        [InlineData("", "")]
        [InlineData("null", "null")]
        [InlineData("Null", "Null")]
        [InlineData("NULL", "NULL")]
        [InlineData("~", "~")]
        public async Task NullArrayInputTestAsync(string optionValue, string expectedNull)
        {
            _options.AddJsonFileWithContent("test.json", "[null, null]");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync("-i", "-c", $"--format-null-as:{optionValue}");
                Assert.Equal(0, status);
                AssertConversionOutputContains($"- {expectedNull}{Environment.NewLine}- {expectedNull}", output);
            }
        }

        [Fact]
        //json2yaml -i --format-null-as
        public async Task FormatNullAsOptionWithoutValueThrowsException()
        {
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync("-i", "--format-null-as");
                Assert.Equal(1, status);
                AssertErrorContains($"--format-null-as cannot be empty", output);
            }
        }

        [Fact]
        public async Task OnlyFormatNullAsOptionIsNotEnough()
        {
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var (status, _) = await Json2YamlExeAsync("--format-null-as");
                Assert.Equal(1, status);
            }
        }

        [Fact]
        public async Task FormatNullAsNotSupportedValueHandlig()
        {
            _options.AddJsonFile("test.json");
            using(new DirPreparator(_options))
            {
                var (status, output) = await Json2YamlExeAsync("--format-null-as:WRONG");
                Assert.Equal(1, status);
                AssertErrorContains("Option 'format-null-as' parsing error. Value ", output);
            }
        }
    }
}