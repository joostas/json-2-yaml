using System.Threading.Tasks;
using Json2Yaml.Integration.Test.Helpers;
using Xunit;
using static Json2Yaml.Integration.Test.Helpers.AssertHelpers;

namespace Json2Yaml.Integration.Test
{

    [Collection("Integration")]
    public class FetchUrlIntegrationTests
    {

        [Trait("Category", "Skip-CI")]
        [Theory]
        [InlineData("--url")]
        [InlineData("-u")]
        //json2yaml -u:existingUrl -o:html.yaml
        public async Task FetchFromCorrectUrlLongVersionTestAsync(string urlOption)
        {
            using(new DirPreparator())
            {
                var (status, output) = await Json2YamlExeAsync($"{urlOption}:{Config.WorkingJsonEndpoint}", "--output:html.yaml");
                Assert.Equal(0, status);
                AssertFileExists("html.yaml");
            }
        }

        [Theory]
        [InlineData("--url")]
        [InlineData("-u")]
        //json2yaml -u:existingUrl
        public async Task FetchFromCorrectUrlWithoutOutputOptionTestAsync(string urlOption)
        {
            using(new DirPreparator())
            {
                var (status, output) = await Json2YamlExeAsync($"{urlOption}:{Config.WorkingJsonEndpoint}");
                Assert.Equal(0, status);
                AssertFileExists(Config.ExpectedFileNameFromWorkingJsonEndpoint);
            }
        }

        [Theory]
        [InlineData("--url")]
        [InlineData("-u")]
        public async Task FetchFromCorrectUrlWithWrongContentTypeFails(string urlOption)
        {
            using(new DirPreparator())
            {
                var (status, output) = await Json2YamlExeAsync($"{urlOption}:{Config.WrongMimeTypeJsonEndpoint}", "-o:html.yaml");
                Assert.Equal(1, status);
                AssertFileNotExist("html.yaml");
                AssertErrorContains("Http response Contetnt type must be json", output);
                AssertErrorContains("Tip: provide flag --skip-content-type-check to ignore this error", output);
            }
        }

        [Theory]
        [InlineData("--url")]
        [InlineData("-u")]
        public async Task FetchFromCorrectUrlWithWrongContentTypePassesWhenSkippingContentTypeCheck(string urlOption)
        {
            using(new DirPreparator())
            {
                var (status, output) = await Json2YamlExeAsync(
                    $"{urlOption}:{Config.WrongMimeTypeJsonEndpoint}", "--skip-content-type-check", "-o:html.yaml");
                Assert.Equal(0, status);
                AssertFileExists("html.yaml");
            }
        }

        [Fact]
        public async Task UseOnlySkipContentTypeCheckFlagFails()
        {
            using(new DirPreparator())
            {
                var (status, output) = await Json2YamlExeAsync("--skip-content-type-check");
                Assert.Equal(1, status);
                AssertErrorContains("When no input options (--url or --input) is provided either --output option with filename or --console-output flag is required.", output);
            }
        }

    }
}