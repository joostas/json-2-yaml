using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using Json2Yaml.Cli;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;
using McMaster.Extensions.CommandLineUtils;
using Xunit;

namespace Json2Yaml.Test;

public class CliInputParserTest
{
    private readonly CliInputParser _sut;
    private readonly Fixture _fixture;

    public CliInputParserTest()
    {
        _fixture = new Fixture();
        _fixture.Customize(new AutoMoqCustomization());
        _fixture.Inject<ICliApplicationHelper>(new CliApplicationHelper());
        _sut = _fixture.Create<CliInputParser>();
    }

    [Fact]
    public void ThrowsArgumentNullException()
    {
        Assert.Throws<ArgumentNullException>(() => _sut.ParseArguments(null));
    }

    [Theory]
    [InlineData("newtonsoft")]
    [InlineData("NEWtonsoft")]
    [InlineData("system")]
    public void JsonFrameworkOptionParsing(string supportedValue)
    {
        // When
        List<string> values = new(){supportedValue};
        var commandOptions = _sut.ParseArguments(new ParsedCommand(new List<string>(), new(){
            new ParsedOption("", "json-framework", CommandOptionType.SingleValue, true, values.AsReadOnly(), supportedValue)
        }));
        // Then
        var jsonFramework = commandOptions.JsonFramework;
        Assert.True(jsonFramework.HasValue);
        Assert.True(jsonFramework.Value == new Json.JsonFramework(supportedValue));
    }

    [Theory]
    [InlineData("other")]
    [InlineData("")]
    public void JsonFrameworkOptionsParsingUnsupportedValuesFails(string unsupportedValue)
    {

        List<string> values = new(){unsupportedValue};
        List<ParsedOption> options = new(){
            new ("", "json-framework", CommandOptionType.SingleValue, true, values.AsReadOnly(), unsupportedValue)
        };
        var exception = Assert.Throws<Json2YamlException>(() => _sut.ParseArguments(new ParsedCommand(new List<string>(), options)));
        Assert.IsType<ArgumentException>(exception.InnerException);
    }

}