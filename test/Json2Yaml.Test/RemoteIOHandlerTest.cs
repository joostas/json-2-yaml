using System;
using System.Threading.Tasks;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;
using Json2Yaml.Utils;
using Moq;
using RichardSzalay.MockHttp;
using Xunit;

namespace Json2Yaml.Test
{
    public class RemoteIOHandlerTest
    {
        private readonly RemoteIOHandler _sut;
        private readonly MockHttpMessageHandler _mockMessageHandler;

        public RemoteIOHandlerTest()
        {
            _mockMessageHandler = new MockHttpMessageHandler();
            using (var mockHttp = _mockMessageHandler)
            {
                _sut = new RemoteIOHandler(mockHttp.ToHttpClient(), Mock.Of<IVersionProvider>());
            }
        }

        [Fact]
        public void Constructor_NullHttpClientThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new RemoteIOHandler(null, Mock.Of<IVersionProvider>()));
        }

        [Fact]
        public void Constructor_NullVersionProviderParameterThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new RemoteIOHandler(new System.Net.Http.HttpClient(), null));
        }
        
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task FetchJson_EmptyImputThrowsArgumentNullExceptionAsync(bool skipContentTypeCheck)
        {
            await Assert.ThrowsAsync<ArgumentNullException>(() => _sut.FetchJsonAsync(null, skipContentTypeCheck));
        }

        [Theory]
        [InlineData("application/json")]
        [InlineData("application/JSON")]
        [InlineData("application/ld+json")]
        [InlineData("text/json")]
        public void AllowedContentTypes(string contentType)
        {
            Assert.True(RemoteIOHandler.IsContentTypeAllowed(contentType, skipContentTypeCheck: false), $"Content-Type {contentType} should be allowed");
        }

        [Theory]
        [InlineData("application/json")]
        [InlineData("application/ld+json")]
        [InlineData("text/json")]
        public void AllowedContentTypesWhenSkippingCheck(string contentType)
        {
            Assert.True(RemoteIOHandler.IsContentTypeAllowed(contentType, skipContentTypeCheck: true), $"Content-Type {contentType} should be allowed when skipping check");
        }

        [Theory]
        [InlineData("text/javascript")]
        [InlineData("text/plain")]
        public void ForbiiddenContentTypes(string contentType)
        {
            Assert.False(RemoteIOHandler.IsContentTypeAllowed(contentType, skipContentTypeCheck: false), $"Content-Type {contentType} should be forbidden");
        }

        [Theory]
        [InlineData("text/javascript")]
        [InlineData("text/plain")]
        public void ForbiiddenContentTypesAllowedIfSkippingContentTypeCheck(string contentType)
        {
            Assert.True(RemoteIOHandler.IsContentTypeAllowed(contentType, skipContentTypeCheck: true), $"Content-Type {contentType} should be allowed when skipping check");
        }

        [Fact]
        public void EmptyContentTypeIsAllowed()
        {
            Assert.True(RemoteIOHandler.IsContentTypeAllowed(null, skipContentTypeCheck: true), $"Empty Content-Type should be allowed");
        }

        [Fact]
        public async Task ForbiddenContentTypeThrows()
        {
            _mockMessageHandler.When("*").Respond("text/plain", "{'name': 'wrong format'}");
            await Assert.ThrowsAsync<Json2YamlException>(() => _sut.FetchJsonAsync("http://test.com/some.json", skipContentTypeCheck: false));
        }

        [Fact]
        public async Task ForbiddenContentTypeIsIgnored()
        {
            _mockMessageHandler.When("*").Respond("application/json", "{'name': 'wrong format'}");
            var result = await _sut.FetchJsonAsync("http://test.com/some.json", skipContentTypeCheck: true);
            Assert.NotEmpty(result);
        }
    }
}