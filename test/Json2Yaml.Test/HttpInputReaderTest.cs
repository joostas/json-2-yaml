using System.Threading;
using System.Threading.Tasks;
using Json2Yaml.Input;
using Json2Yaml.Interfaces;
using Moq;
using Xunit;

namespace Json2Yaml.Test
{
    public class HttpInputReaderTest
    {
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public async Task SkipContentTypeCheckIsPassedToRemoteIOHandler(bool skipContentTypeCheck)
        {
            var remoteInputReaderMock = new Mock<IRemoteIOHandler>();
            var sut = new HttpInputReader(remoteInputReaderMock.Object, skipContentTypeCheck);
            var result = await sut.ReadJsonAsync("http://address.com", new CancellationToken());
            remoteInputReaderMock.Verify((o) => o.FetchJsonAsync("http://address.com", skipContentTypeCheck, It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}