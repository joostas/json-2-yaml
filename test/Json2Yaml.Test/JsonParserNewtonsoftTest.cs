using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;
using Json2Yaml.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace Json2Yaml.Test;

public class NewtonsoftJsonParserTest
{
    protected readonly IJsonParser _sut;

    public NewtonsoftJsonParserTest()
    {
        _sut = new NewtonsoftJsonParser();
    }

    [Fact]
    public void ParseFloat()
    {
        var result = _sut.ParseJson("-122.422003528252475");
        Assert.Equal(-122.422003528252475M, result);
    }

    [Fact]
    public void ParseDate()
    {
        var iso8601DateString = "2019-10-06T15:06:07Z";
        var result = _sut.ParseJson($"\"{iso8601DateString}\"");
        var expectedDateTime = new DateTime(2019, 10, 06, 15, 06, 07, DateTimeKind.Utc);
        Assert.Equal(expectedDateTime, result);
        // same check, but using parser
        expectedDateTime = DateTime.Parse(iso8601DateString, null, DateTimeStyles.AdjustToUniversal);
        Assert.Equal(expectedDateTime, result);
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData("   ")]
    public void JsonParseThrowsArgumentException(string emptyInput)
    {
        Assert.Throws<ArgumentException>(() => _sut.ParseJson(emptyInput));
    }

    [Fact]
    public void ThrowsJson2YamlExceptionForInvalidJsonContent()
    {
        Assert.Throws<Json2YamlException>(() => _sut.ParseJson("some text. Nothing useful"));
    }

    [Fact]
    public void ParsesCorrectJsonInput()
    {
        var result = _sut.ParseJson("""
        {
            "input": "value",
            "output": 15
        }
        """);
        Assert.NotNull(result);
    }

    [Fact]
    public void ParseEmptyList()
    {
        var result = _sut.ParseJson("[]");
        Assert.NotNull(result);
    }

    [Fact]
    public void ParseJsonObjectWithComments()
    {
        var result = _sut.ParseJson("""
        //comments {[ goes here
        {
            "test": "value"
        }
        """);
        Assert.NotNull(result);
    }

    [Fact]
    public void ParseJsonArrayWithComments()
    {
        var result = _sut.ParseJson("""
        // comments {[ goes here
        [
            "test", "value"
        ]
        """);
        Assert.NotNull(result);
    }

    [Fact]
    public void ParseListOfPrimitives()
    {
        var result = _sut.ParseJson("[\"test\", \"test1\", \"test2\"]");
        Assert.NotNull(result);
    }

    [Theory]
    [InlineData("159")]
    [InlineData("-159")]
    [InlineData("0")]
    [InlineData("2147483649")] //int.MaxValue + 1
    public void ParseWholeNumber(string number)
    {
        var result = _sut.ParseJson(number);
        Assert.Equal(decimal.Parse(number), result);
    }

    [Theory]
    [InlineData("true")]
    [InlineData("false")]
    public void ParseBoolean(string boolString)
    {
        var result = _sut.ParseJson(boolString);
        Assert.Equal(bool.Parse(boolString), result);
    }

    [Theory]
    [InlineData("\"some_text\"")]
    [InlineData("\"💪💪💪\"")]
    [InlineData("\"some5\"")]
    [InlineData("\"some long text\"")]
    [InlineData("\"\"")]
    public void ParseString(string justString)
    {
        var result = _sut.ParseJson(justString);
        Assert.Equal(justString.Trim('"'), result);
    }

    [Fact]
    public void ParseNull()
    {
        var result = _sut.ParseJson("null");
        Assert.Null(result);
    }

    [Theory]
    [InlineData("True")]
    [InlineData("False")]
    public void UpperCaseBoolDoesntParse(string wrongBoolString) =>
        Assert.Throws<Json2YamlException>(() => _sut.ParseJson(wrongBoolString));

    [Fact]
    public void UpperCaseNullDoesntParse() =>
        Assert.Throws<Json2YamlException>(() => _sut.ParseJson("Null"));

    [Fact]
    public void ParseMixedArrayWithObject()
    {
        var json = """["ab", {"b":"c"}, 18]""";
        var result = _sut.ParseJson(json);
        var array = (List<object>)result;
        var type = array.ElementAt(1).GetType();
        var noJObjectTypes = array.All(o => o.GetType() != typeof(JObject));
        Assert.True(noJObjectTypes, "After conversion array should not contain JObject types");
    }

    [Fact]
    public void ParseMixedArrayWithInnerArray()
    {
        var json = """["ab", ["b","c"], 18]""";
        var result = _sut.ParseJson(json);
        var array = (List<object>)result;
        var noJArrayTypes = array.All(o => o.GetType() != typeof(JArray));
        Assert.True(noJArrayTypes, "After conversion array should not contain JArray types");
    }

    [Fact]
    public void ParseMixedArrayWithInnerArrayAndObject()
    {
        var json = """["ab", ["b","c"], {"a": 18}, 20]""";
        var result = _sut.ParseJson(json);
        //var array = Convert.ChangeType(result, _arrayType);
        var array = (List<object>)result;
        var noJArrayTypes = array.All(o => o.GetType() != typeof(JArray));
        var noJObjectTypes = array.All(o => o.GetType() != typeof(JObject));
        Assert.True(noJArrayTypes, "After conversion array should not contain JArray types");
        Assert.True(noJObjectTypes, "After conversion array should not contain JObject types");
    }

    [Fact]
    public void ParseMixedArrayWithInnerArrayMaxLevelReached()
    {
        // Todo cleanup
        //This is 4 level depth. Must throw.
        if (this is NewtonsoftJsonParserTest)
        {
            return; //Currently SimpleJsonParser and JsonParser treats recursion level differently.
        }
        var nestedToMutchJson = """["ab", ["b","c", [[1, 2], 3]], 18]""";
        var ex = Assert.Throws<Json2YamlException>(() => _sut.ParseJson(nestedToMutchJson));
        Assert.Contains("Json structure is not supported: nested level in array is too deep.", ex.Message);
    }

    [Fact]
    public void ParseMixedArrayWithInnerArrayMaxLevelNotReached()
    {
        //This is only 3 level depth structure. Must pass. Current limit is 3.
        var nestedJson = """["ab", ["b","c", [8, 3]], [1, 2], [0, 0], [0, 0], 18]""";
        var result = _sut.ParseJson(nestedJson);
        var array = (List<object>)result;
        var noJArrayTypes = array.All(o => o.GetType() != typeof(JArray));
        var noJObjectTypes = array.All(o => o.GetType() != typeof(JObject));
        Assert.True(noJArrayTypes, "After conversion array should not contain JArray types");
        Assert.True(noJObjectTypes, "After conversion array should not contain JObject types");
    }

    [Theory]
    [InlineData("[null, 5, 4]", 0, 1)]
    [InlineData("[3, 5, null]", 2, 0)]
    [InlineData("[null, 5, null]", 2, 1)]
    [InlineData("""[null, 5, "3"]""", 0, 2)]
    public void ParseMixedArrayWithNull(string json, int nullIndex, int nonNullIndex)
    {
        var result = _sut.ParseJson(json);
        var resultArray = (List<object>)result;
        Assert.Null(resultArray.ElementAt(nullIndex));
        Assert.NotNull(resultArray.ElementAt(nonNullIndex));
    }
}
