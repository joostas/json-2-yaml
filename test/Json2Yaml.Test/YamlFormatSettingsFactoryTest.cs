using System;
using Json2Yaml.Cli;
using Json2Yaml.Yaml;
using Xunit;

namespace Json2Yaml.Test
{
    public class YamlFormatSettingsFactoryTest
    {

        [Fact]
        public void CreateMethodHandlesNull()
        {
            var sut = new YamlFormatSettingsFactory();
            Assert.Throws<ArgumentNullException>(() => sut.Create(null));
        }

        [Theory]
        [InlineData("empty")]
        [InlineData("null")]
        [InlineData("Null")]
        [InlineData("~")]
        public void NullFormatOptionsAreExtracted(string nullFormatString)
        {
            var sut = new YamlFormatSettingsFactory();
            var givenNullFormat = new NullFormat(nullFormatString);
            var commanOptions = new CommandOptions()
            {
                NullFormat = new OptionInfo<NullFormat>(true, givenNullFormat),
            };
            var result = sut.Create(commanOptions);
            Assert.Equal(givenNullFormat, result.NullFormat);
        }

        [Fact]
        public void WhenNullFormatOptionIsNotDefinedDefaultValueIsUsed()
        {
            var sut = new YamlFormatSettingsFactory();
            var emptyOptions = new CommandOptions()
            {
                NullFormat = new OptionInfo<NullFormat>(false, default),
            };
            var result = sut.Create(emptyOptions);
            Assert.Equal(NullFormat.Default, result.NullFormat);
        }
    }
}