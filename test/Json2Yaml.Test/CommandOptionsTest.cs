using Json2Yaml.Cli;
using Xunit;

namespace Json2Yaml.Test
{
    public class CommandOptionsTest
    {
        [Fact]
        public void NoOptionsIsAnyNonDecorativeDefined_ReturnsFalse()
        {
            var sut = new CommandOptions();
            Assert.False(sut.IsAnyNonDecorativeDefined());
        }

        [Fact]
        public void SomeNotDefinedOption_ReturnsFalse()
        {
            var sut = new CommandOptions()
            {
                Url = new OptionInfo<string>(false, null),
            };
            Assert.False(sut.IsAnyNonDecorativeDefined());
        }

        [Fact]
        public void SomeOptionsIsAnyNonDecorativeDefined_ReturnsTrue()
        {
            var sut = new CommandOptions()
            {
                MaxSize = new OptionInfo<int?>(true, 5),
            };
            Assert.True(sut.IsAnyNonDecorativeDefined());
        }

        [Fact]
        public void SomeEmptyOptionIsAnyNonDecorativeDefined_ReturnsTrue()
        {
            var sut = new CommandOptions()
            {
                Url = new OptionInfo<string>(true, null),
            };
            Assert.True(sut.IsAnyNonDecorativeDefined());
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void VerboseOptionDoesNotMarkedAsOptionDefined(bool value)
        {
            var sut = new CommandOptions()
            {
                Verbose = new OptionInfo<bool>(true, value),
            };
            Assert.False(sut.IsAnyNonDecorativeDefined());
        }

        [Fact]
        public void IsOnlyVersionProvided()
        {
            var sut = new CommandOptions()
            {
                Version = new OptionInfo<bool>(true, true),
            };
            Assert.True(sut.IsOnlyVersionProvided(), "Should be marked that only version is provided");
        }

        [Fact]
        public void NotOnlyVersionProvided()
        {
            var sut = new CommandOptions()
            {
                Version = new OptionInfo<bool>(true, true),
                Verbose = new OptionInfo<bool>(true, true),
            };
            Assert.False(sut.IsOnlyVersionProvided(), "Should be marked that not only version is provided");
        }

        [Fact]
        public void NotOnlyVersionProvidedWenVersionIsMissing()
        {
            var sut = new CommandOptions()
            {
                Verbose = new OptionInfo<bool>(true, true),
                Input = new OptionInfo<string>(true, "test.json"),
            };
            Assert.False(sut.IsOnlyVersionProvided(), "Should be marked that not only version is provided");
        }
    }
}