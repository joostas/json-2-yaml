using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using Boxed.Mapping;
using Json2Yaml.Cli;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;
using Json2Yaml.Utils;
using McMaster.Extensions.CommandLineUtils.Abstractions;
using Moq;
using Xunit;

namespace Json2Yaml.Test;

public class Json2YamlCliTest
{
    private readonly Json2YamlCli _sut;
    private readonly Fixture _fixture;
    private readonly IOutputWriterFactory _outputWriterFactory;

    public Json2YamlCliTest()
    {
        DependencyRegistrer.ConfigureDependencies(null);
        _fixture = new Fixture();
        _fixture.Customize(new AutoMoqCustomization());
        _outputWriterFactory = Mock.Of<IOutputWriterFactory>();
        _fixture.Inject(_outputWriterFactory);
        _sut = _fixture.Create<Json2YamlCli>();
        _sut.Initialize();
    }

    [Fact]
    public void IncorrectInputParameterThrowsJson2YamlException()
    {
        var incorrectArgs = new string[]{"-i", "/path/to/file"};
        _sut.Initialize();
        Assert.Throws<Json2YamlException>(() => _sut.ParseArguments(incorrectArgs));
    }

    [Fact]
    public void CannotParseArgumentsIfNotInitialized()
    {
        var sut = _fixture.Create<Json2YamlCli>();
        Assert.Throws<InvalidOperationException>(() => sut.ParseArguments(Array.Empty<string>()));
    }

    [Fact]
    public async Task CannotRunIfNotInitialized()
    {
        var sut = _fixture.Create<Json2YamlCli>();
        await Assert.ThrowsAsync<InvalidOperationException>(() => sut.RunAsync());
    }

    [Fact]
    public async Task CannotRunIfNotParsed()
    {
        await Assert.ThrowsAsync<InvalidOperationException>(() => _sut.RunAsync());
    }

    [Fact]
    public void ConstructorThrowsNullArgumentExceptionForJson2YamlConverter()
    {
        Assert.Throws<ArgumentNullException>(() => new Json2YamlCli(
            json2YamlConverter:      null,
            ioHandler:               Mock.Of<IIOHandler>(),
            inputFactory:            Mock.Of<IInputReaderFactory>(),
            outputWriterFactory:     Mock.Of<IOutputWriterFactory>(),
            cliInitializer:          Mock.Of<ICliInitializer>(),
            cliParser:               Mock.Of<ICliInputParser>(),
            cliInterpreterFactory:   Mock.Of<Func<CommandOptions, ICliOptionsInterpreter>>(),
            mapper:                  Mock.Of<IImmutableMapper<ParseResult, ParsedCommand>>()
        ));
    }

    [Fact]
    public void ConstructorThrowsNullArgumentExceptionForIOHandler()
    {
        Assert.Throws<ArgumentNullException>(() => new Json2YamlCli(
            json2YamlConverter:      Mock.Of<IJson2YamlConverter>(),
            ioHandler:               null,
            inputFactory:            Mock.Of<IInputReaderFactory>(),
            outputWriterFactory:     Mock.Of<IOutputWriterFactory>(),
            cliInitializer:          Mock.Of<ICliInitializer>(),
            cliParser:               Mock.Of<ICliInputParser>(),
            cliInterpreterFactory:   Mock.Of<Func<CommandOptions, ICliOptionsInterpreter>>(),
            mapper:                  Mock.Of<IImmutableMapper<ParseResult, ParsedCommand>>()
            ));
    }

    [Fact]
    public void ConstructorThrowsNullArgumentExceptionForInputFactory()
    {
        Assert.Throws<ArgumentNullException>(() => new Json2YamlCli(
            json2YamlConverter:      Mock.Of<IJson2YamlConverter>(),
            ioHandler:               Mock.Of<IIOHandler>(),
            inputFactory:            null,
            outputWriterFactory:     Mock.Of<IOutputWriterFactory>(),
            cliInitializer:          Mock.Of<ICliInitializer>(),
            cliParser:               Mock.Of<ICliInputParser>(),
            cliInterpreterFactory:   Mock.Of<Func<CommandOptions, ICliOptionsInterpreter>>(),
            mapper:                  Mock.Of<IImmutableMapper<ParseResult, ParsedCommand>>()
            ));
    }

    [Fact]
    public void ConstructorThrowsNullArgumentExceptionForIOWriterParameter()
    {
        Assert.Throws<ArgumentNullException>(() => new Json2YamlCli(
            json2YamlConverter:      Mock.Of<IJson2YamlConverter>(),
            ioHandler:               Mock.Of<IIOHandler>(),
            inputFactory:            Mock.Of<IInputReaderFactory>(),
            outputWriterFactory:     null,
            cliInitializer:          Mock.Of<ICliInitializer>(),
            cliParser:               Mock.Of<ICliInputParser>(),
            cliInterpreterFactory:   Mock.Of<Func<CommandOptions, ICliOptionsInterpreter>>(),
            mapper:                  Mock.Of<IImmutableMapper<ParseResult, ParsedCommand>>()
            ));
    }

    [Fact]
    public void ConstructorThrowsNullArgumentExceptionForCliInitializer()
    {
        Assert.Throws<ArgumentNullException>(() => new Json2YamlCli(
            json2YamlConverter:      Mock.Of<IJson2YamlConverter>(),
            ioHandler:               Mock.Of<IIOHandler>(),
            inputFactory:            Mock.Of<IInputReaderFactory>(),
            outputWriterFactory:     Mock.Of<IOutputWriterFactory>(),
            cliInitializer:          null,
            cliParser:               Mock.Of<ICliInputParser>(),
            cliInterpreterFactory:   Mock.Of<Func<CommandOptions, ICliOptionsInterpreter>>(),
            mapper:                  Mock.Of<IImmutableMapper<ParseResult, ParsedCommand>>()
            ));
    }

    [Fact]
    public void ConstructorThrowsNullArgumentExceptionForCliInputParser()
    {
        Assert.Throws<ArgumentNullException>(() => new Json2YamlCli(
            json2YamlConverter:      Mock.Of<IJson2YamlConverter>(),
            ioHandler:               Mock.Of<IIOHandler>(),
            inputFactory:            Mock.Of<IInputReaderFactory>(),
            outputWriterFactory:     Mock.Of<IOutputWriterFactory>(),
            cliInitializer:          Mock.Of<ICliInitializer>(),
            cliParser:               null,
            cliInterpreterFactory:   Mock.Of<Func<CommandOptions, ICliOptionsInterpreter>>(),
        mapper:                      Mock.Of<IImmutableMapper<ParseResult, ParsedCommand>>()
            ));
    }

    [Fact]
    public void ConstructorThrowsNullArgumentExceptionForExecutor()
    {
        Assert.Throws<ArgumentNullException>(() => new Json2YamlCli(
            json2YamlConverter:      Mock.Of<IJson2YamlConverter>(),
            ioHandler:               Mock.Of<IIOHandler>(),
            inputFactory:            Mock.Of<IInputReaderFactory>(),
            outputWriterFactory:     Mock.Of<IOutputWriterFactory>(),
            cliInitializer:          Mock.Of<ICliInitializer>(),
            cliParser:               Mock.Of<ICliInputParser>(),
            cliInterpreterFactory:   null,
            mapper:                  Mock.Of<IImmutableMapper<ParseResult, ParsedCommand>>()
            ));
    }

    [Fact]
    public void ConstructorThrowsNullArgumentExceptionForNullMapper()
    {
        Assert.Throws<ArgumentNullException>(() => new Json2YamlCli(
            json2YamlConverter:      Mock.Of<IJson2YamlConverter>(),
            ioHandler:               Mock.Of<IIOHandler>(),
            inputFactory:            Mock.Of<IInputReaderFactory>(),
            outputWriterFactory:     Mock.Of<IOutputWriterFactory>(),
            cliInitializer:          Mock.Of<ICliInitializer>(),
            cliParser:               Mock.Of<ICliInputParser>(),
            cliInterpreterFactory:   Mock.Of<Func<CommandOptions, ICliOptionsInterpreter>>(),
            mapper:                  null
            ));
    }

    [Fact]
    public void MultipleInputOptionsThrowsJson2YamlException()
    {
        Assert.Throws<Json2YamlException>(() => 
            _sut.ParseArguments(new []{"-i:file.json", "-i:file2.json"}));
    }

    [Fact]
    public async Task UrlOptionRequiresOutputOption_Pass()
    {
        var urlHelper = Mock.Of<IUrlHelper>((o) => o.CheckIfUrlCorrect(It.IsAny<string>()) == true);
        _fixture.Inject(urlHelper);
        var outputWriterFactory = Mock.Of<IOutputWriterFactory>(
            (o) => o.Create(It.IsAny<bool>(), It.IsAny<bool>()) == Mock.Of<IOutputWriter>());
        _fixture.Inject(outputWriterFactory);
        _fixture.Inject<IImmutableMapper<ParseResult, ParsedCommand>>(new ParsedCommandMapper());
        _fixture.Inject<ICliApplicationHelper>(new CliApplicationHelper());
        _fixture.Inject<ICliInitializer>(new Cli.CliInitializer(Mock.Of<IVersionProvider>(), new Constants()));
        var sut = _fixture.Create<Json2YamlCli>();
        sut.Initialize();
        sut.ParseArguments(new[]{"-u:http://test.com/a.json", "-o:output.yaml"});
        var result = await sut.RunAsync();
        Assert.Equal(0, result);
    }

    [Theory]
    [InlineData("-c")]
    [InlineData("--console-output")]
    public void ConsoleOptionPreventsWritingInfoToConsole(string consoleOption)
    {
        _fixture.Inject<IImmutableMapper<ParseResult, ParsedCommand>>(new ParsedCommandMapper());
        _fixture.Inject<ICliApplicationHelper>(new CliApplicationHelper());
        _fixture.Inject<ICliInitializer>(new Cli.CliInitializer(Mock.Of<IVersionProvider>(), new Constants()));
        var sut = _fixture.Create<Json2YamlCli>();
        sut.Initialize();
        sut.ParseArguments(new string[]{"-i:input.json", consoleOption});
        Assert.False(sut.CanWriteInfoToConsole, "Should not be able to write to console");
    }

    [Fact]
    public void ConsoleOptionMissingLetsWriteInfoToConsole()
    {
        _fixture.Inject<ICliApplicationHelper>(new CliApplicationHelper());
        var inputParser = Mock.Of<ICliInputParser>(o => o.CanWriteInfoToConsole(It.IsAny<List<ParsedOption>>()) == true);
        _fixture.Inject(inputParser);
        _fixture.Inject<IImmutableMapper<ParseResult, ParsedCommand>>(new ParsedCommandMapper());
        _fixture.Inject<ICliInitializer>(new Cli.CliInitializer(Mock.Of<IVersionProvider>(), new Constants()));
        var sut = _fixture.Create<Json2YamlCli>();
        sut.Initialize();
        sut.ParseArguments(new string[]{"-i:input.json"});
        Assert.True(sut.CanWriteInfoToConsole, "Should be able to write to console");
    }
}