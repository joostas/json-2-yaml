using System;
using Json2Yaml.Exceptions;
using Json2Yaml.Utils;
using Xunit;

namespace Json2Yaml.Test
{
    public class UrlHelperTest
    {
        private readonly UrlHelper _sut;

        public UrlHelperTest()
        {
            _sut = new UrlHelper();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("    ")]
        [InlineData("")]
        public void CheckIfUrlCorrect_EmptyImputTreatedAsIncorrect(string empty)
        {
            var result = _sut.CheckIfUrlCorrect(empty);
            Assert.False(result);
        }

        [Theory]
        [InlineData("http://api.test.calls/swagger.json")]
        [InlineData("https://api.test.calls/swagger.json")]
        [InlineData("api.test.calls/swagger.json")]
        public void CheckIfUrlCorrect_CorrectValues(string correctUrl)
        {
            var result = _sut.CheckIfUrlCorrect(correctUrl);
            Assert.True(result);
        }

        [Theory]
        [InlineData("htp://api.test.calls/swagger.json")]
        [InlineData("amqp://api.test.calls/swagger.json")]
        public void CheckIfUrlCorrect_IncorrectValues(string correctUrl)
        {
            var result = _sut.CheckIfUrlCorrect(correctUrl);
            Assert.False(result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("    ")]
        [InlineData("")]
        public void ExtractFileNameThorwsOnEmptyInput(string emptyInput)
        {
            Assert.Throws<ArgumentException>(() => _sut.ExtractFileName(emptyInput));
        }

        [Theory]
        [InlineData("test.json")]
        [InlineData("some/path/test.json")]
        public void ExtractFileNameThrowsOnFiles(string fileName)
        {
            Assert.Throws<ArgumentException>(() => _sut.ExtractFileName(fileName));
        }

        [Fact]
        public void ExtractFileNameThrowsOnMalformedUrls()
        {
            Assert.Throws<ArgumentException>(() => _sut.ExtractFileName("http:/test/test/input.json"));
        }

        [Fact]
        public void ExtractFileNameThrowsOnUrlsWithEmptyPath()
        {
            Assert.Throws<Json2YamlException>(() => _sut.ExtractFileName("http://test.com/"));
        }

        [Theory]
        [InlineData("http://test.com/info.json", "info.yaml")]
        [InlineData("http://test.com/info", "info.yaml")]
        [InlineData("http://test.com/info.json?filter=something", "info.yaml")]
        public void NameExtractedFromUrl(string url, string expected)
        {
            var result = _sut.ExtractFileName(url);
            Assert.Equal(expected, result);
        }
    }
}