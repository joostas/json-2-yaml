using System;
using System.IO;
using Json2Yaml.Interfaces;
using Json2Yaml.Output;
using Moq;
using Xunit;

namespace Json2Yaml.Test
{
    public class OutputWritersTest
    {
        private readonly TextWriter _redirected;
        private readonly TextWriter _original;
        public OutputWritersTest()
        {
            _original = Console.Error;
            _redirected = new StringWriter();
            Console.SetError(_redirected);
        }

        [Fact]
        public void TestVerboseErrorConsoleWriter()
        {
            var sut = new ConsoleOutputWriter(true);
            sut.WriteError(new Exception("Error message"));
            var errorText = _redirected.ToString();
            Assert.Contains("Error message", errorText);
            Assert.Contains("Please check for existing issues https://gitlab.com/joostas/json-2-yaml/issues and register new one if needed here:", errorText);
            Assert.Contains("https://gitlab.com/joostas/json-2-yaml/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=", errorText);
        }

        [Fact]
        public void TestVerboseErrorFileWriter()
        {
            var sut = new FileOutputWriter(Mock.Of<IIOHandler>(), true);
            sut.WriteError(new Exception("Error message"));
            var errorText = _redirected.ToString();
            Assert.Contains("Error message", errorText);
            Assert.Contains("Please check for existing issues https://gitlab.com/joostas/json-2-yaml/issues and register new one if needed here:", errorText);
            Assert.Contains("https://gitlab.com/joostas/json-2-yaml/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=", errorText);
        }

        [Fact]
        public void TestErrorConsoleWriter()
        {
            var sut = new ConsoleOutputWriter(false);
            sut.WriteError(new Exception("Error message"));
            var errorText = _redirected.ToString();
            Assert.Contains("Error message", errorText);
            Assert.DoesNotContain("Please check for existing issues https://gitlab.com/joostas/json-2-yaml/issues and register new one if needed here:", errorText);
            Assert.DoesNotContain("https://gitlab.com/joostas/json-2-yaml/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=", errorText);
        }

        [Fact]
        public void TestErrorFileWriter()
        {
            var sut = new FileOutputWriter(Mock.Of<IIOHandler>(), false);
            sut.WriteError(new Exception("Error message"));
            var errorText = _redirected.ToString();
            Assert.Contains("Error message", errorText);
            Assert.DoesNotContain("Please check for existing issues https://gitlab.com/joostas/json-2-yaml/issues and register new one if needed here:", errorText);
            Assert.DoesNotContain("https://gitlab.com/joostas/json-2-yaml/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=", errorText);
        }

        ~OutputWritersTest()
        {
            Console.SetError(_original);
            _redirected?.Dispose();
        }
    }
}