using System;
using System.Dynamic;
using Json2Yaml.Yaml;
using Xunit;
using Json2Yaml.Interfaces;

namespace Json2Yaml.Test
{
    public class YamlProducerTest
    {
        private readonly YamlProducer _sut;
        private readonly IYamlFormatSettings _yamlFormatSettings;

        public YamlProducerTest()
        {
            _yamlFormatSettings = new YamlFormatSettings(new NullFormat("empty"));
            _sut = new YamlProducer();
        }

        [Fact]
        public void EmptyInputIsAllowed()
        {
            var result = _sut.ProduceYaml(null, _yamlFormatSettings);
            Assert.Equal($"--- {Environment.NewLine}", result);
        }

        [Fact]
        public void CorrectInputProducesJson()
        {
            dynamic address = new ExpandoObject();
            address.Street = "Konstitucijos pr.";
            address.City = "Vilnius";
            dynamic expando = new ExpandoObject();
            expando.Name = "Name";
            expando.Value = 125;
            expando.Address = address;

            var res = _sut.ProduceYaml(expando, _yamlFormatSettings);
            Assert.NotNull(res);
        }
    }
}