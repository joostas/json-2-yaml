using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Json2Yaml.Exceptions;
using Json2Yaml.Interfaces;
using Json2Yaml.Utils;
using Moq;
using Xunit;

namespace Json2Yaml.Test
{
    public class IOHandlerTest
    {
        [Fact]
        public void Constructor_ArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new IOHandler(null, null));
            Assert.Throws<ArgumentNullException>(() => new IOHandler(null, Mock.Of<IConsoleRedirectionMarker>()));
            Assert.Throws<ArgumentNullException>(() => new IOHandler(Mock.Of<IFileSystem>(), null));
            Assert.Throws<ArgumentNullException>(() => new IOHandler(null));
        }
        
        [Fact]
        public async Task ReadFile_ReadsExistingFileContentTestAsync()
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystemRoot = Path.GetPathRoot(currentDirectory);
            var existingFileName = "myFile.txt";
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(fileSystemRoot, existingFileName), new MockFileData("Testing is meh.") },
                { Path.Combine(fileSystemRoot, "demo", "jQuery.js"), new MockFileData("some js") },
                { Path.Combine(fileSystemRoot, "demo", "image.gif"), new MockFileData(new byte[] { 0x12, 0x34, 0x56, 0xd2 }) }
            });
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            var result = await sut.ReadFileAsync(Path.Combine(fileSystemRoot, existingFileName), default);
            Assert.Equal("Testing is meh.", result);
        }

        [Fact]
        public async Task ReadFile_NonExistingFileThrowsJson2YamlException()
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystemRoot = Path.GetPathRoot(currentDirectory);
            var notExistingFileName = "myFile.txt";
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(fileSystemRoot, "file.txt"), new MockFileData("Testing is meh.") },
                { Path.Combine(fileSystemRoot, "demo", "jQuery.js"), new MockFileData("some js") },
                { Path.Combine(fileSystemRoot, "demo", "image.gif"), new MockFileData(new byte[] { 0x12, 0x34, 0x56, 0xd2 }) }
            });
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            await Assert.ThrowsAsync<Json2YamlException>(() => sut.ReadFileAsync(Path.Combine(fileSystemRoot, notExistingFileName), default));
        }

        [Fact]
        public async Task WriteFile_WritesToExistingFileTestAsync()
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystemRoot = Path.GetPathRoot(currentDirectory);
            var basePath = Path.Combine(fileSystemRoot, "tmp"); //this is needed for System.IO>Abstractions to work on linux. //this is needed for System.IO>Abstractions to work on linux.
            var existingFileName = "myFile.txt";
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(basePath, existingFileName), new MockFileData("Testing is meh.") },
                { Path.Combine(basePath, "demo", "jQuery.js"), new MockFileData("some js") },
                { Path.Combine(basePath, "demo", "image.gif"), new MockFileData(new byte[] { 0x12, 0x34, 0x56, 0xd2 }) }
            });
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            await sut.WriteFileAsync(Path.Combine(basePath, existingFileName), "test test test", default);
            var result = await sut.ReadFileAsync(Path.Combine(basePath, existingFileName), default);
            Assert.Equal("test test test", result);
        }

        [Fact]
        public async Task WriteFile_WritesToNonExistingFileTest()
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystemRoot = Path.GetPathRoot(currentDirectory);
            var basePath = Path.Combine(fileSystemRoot, "tmp"); //this is needed for System.IO>Abstractions to work on linux.
            var nonExistingFileName = "myFile.txt";
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(basePath, "fileName.txt"), new MockFileData("Testing is meh.") },
                { Path.Combine(basePath, "demo", "jQuery.js"), new MockFileData("some js") },
                { Path.Combine(basePath, "demo", "image.gif"), new MockFileData(new byte[] { 0x12, 0x34, 0x56, 0xd2 }) }
            });
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            await sut.WriteFileAsync(Path.Combine(basePath, nonExistingFileName), "test test test", default);
            var result = await sut.ReadFileAsync(Path.Combine(basePath, nonExistingFileName), default);
            Assert.Equal("test test test", result);
        }

        [Fact]
        public async Task WriteFile_WriteToReadOnlyFileThrowsJson2YamlException()
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystemRoot = Path.GetPathRoot(currentDirectory);
            var basePath = Path.Combine(fileSystemRoot, "tmp"); //this is needed for System.IO>Abstractions to work on linux. //this is needed for System.IO>Abstractions to work on linux.
            var readOnlyFileName = Path.Combine(basePath, "fileName.txt");
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { readOnlyFileName, new MockFileData("Testing is meh.") },
                { Path.Combine(basePath, "demo", "jQuery.js"), new MockFileData("some js") },
                { Path.Combine(basePath, "demo", "image.gif"), new MockFileData(new byte[] { 0x12, 0x34, 0x56, 0xd2 }) }
            });
            //mark file as readOnly
            fileSystem.File.SetAttributes(readOnlyFileName, FileAttributes.ReadOnly);
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            await Assert.ThrowsAsync<Json2YamlException>(() =>
                sut.WriteFileAsync(Path.Combine(basePath, readOnlyFileName), "test test test", default));
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public async Task ReadFileWithEmptyPath_ThrowsArgumentException(string emptyPath)
        {
            var sut = new IOHandler(Mock.Of<IConsoleRedirectionMarker>());
            await Assert.ThrowsAsync<ArgumentException>(() => sut.ReadFileAsync(emptyPath, default));
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public async Task WriteFileWithEmptyPath_ThrowsArgumentException(string emptyPath)
        {
            var sut = new IOHandler(Mock.Of<IConsoleRedirectionMarker>());
            await Assert.ThrowsAsync<ArgumentException>(() => sut.WriteFileAsync(emptyPath, "test test test", default));
        }

        [Fact]
        public async Task WriteFileWithEmptyContent_DoesntThrow()
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystemRoot = Path.GetPathRoot(currentDirectory);
            var basePath = Path.Combine(fileSystemRoot, "tmp"); //this is needed for System.IO>Abstractions to work on linux. //this is needed by System.IO>Abstractions to work on linux.
            var fileName = Path.Combine(basePath, "fileName.txt");
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { fileName, new MockFileData("Testing is meh.") },
                { Path.Combine(basePath, "demo", "jQuery.js"), new MockFileData("some js") },
                { Path.Combine(basePath, "demo", "image.gif"), new MockFileData(new byte[] { 0x12, 0x34, 0x56, 0xd2 }) }
            });
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            var res = await Record.ExceptionAsync(() => sut.WriteFileAsync(fileName, (string)null, default));
            Assert.Null(res);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public void FindFiles_ThrowsArgumentExceptionForEmptyPath(string emptyPath)
        {
            var sut = new IOHandler(Mock.Of<IConsoleRedirectionMarker>());
            Assert.Throws<ArgumentException>(() => sut.FindFiles(emptyPath, ".json"));
        }

        [Fact]
        public void FindFiles_ThrowsArgumentNullExceptionForNullExtension()
        {
            var sut = new IOHandler(Mock.Of<IConsoleRedirectionMarker>());
            Assert.Throws<ArgumentNullException>(() => sut.FindFiles("file.txt", null));
        }

        [Theory]
        [InlineData(null)]
        [InlineData(" ")]
        [InlineData("incorrectPathToFile")]
        public void IsSameExtension_IncorrectPathReturnsFalse(string wrongPath)
        {
            var sut = new IOHandler(Mock.Of<IConsoleRedirectionMarker>());
            var result = sut.IsSameExtension(wrongPath, ".json");
            Assert.False(result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(" ")]
        [InlineData("incorrectPathToFile")]
        public void IsSameExtension_EmptyExtensionReturnsFalse(string emptyExtension)
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystemRoot = Path.GetPathRoot(currentDirectory);
            var fileName = Path.Combine(fileSystemRoot, "fileName.txt");
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { fileName, new MockFileData("Testing is meh.") },
                { Path.Combine(fileSystemRoot, "demo", "jQuery.js"), new MockFileData("some js") },
                { Path.Combine(fileSystemRoot, "demo", "image.gif"), new MockFileData(new byte[] { 0x12, 0x34, 0x56, 0xd2 }) }
            });
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            var result = sut.IsSameExtension(fileName, emptyExtension);
            Assert.False(result);
        }

        [Theory]
        [InlineData(".json")]
        [InlineData(".JSON")]
        public void IsSameExtension_ReturnsTrue(string extension)
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystemRoot = Path.GetPathRoot(currentDirectory);
            var fileName = Path.Combine(fileSystemRoot, "fileName.json");
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { fileName, new MockFileData("Testing is meh.") },
            });
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            var result = sut.IsSameExtension(fileName, extension);
            Assert.True(result);
        }

        [Fact]
        public void FileSizeInMBTest()
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystemRoot = Path.GetPathRoot(currentDirectory);
            var fileName = Path.Combine(fileSystemRoot, "fileName.json");
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { fileName, new MockFileData("".PadRight(10*1024*1024)) },
            });
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            var result = sut.GetFileSize(fileName);
            Assert.Equal(10, result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("  ")]
        public void GetFileSize_NullArgumentThrows(string emptyPath)
        {
            var sut = new IOHandler(Mock.Of<IConsoleRedirectionMarker>());
            Assert.Throws<ArgumentException>(() => sut.GetFileSize(emptyPath));
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void IsInputRedirectedTest(bool isRedirected)
        {
            var sut = new IOHandler(Mock.Of<IFileSystem>(),
                Mock.Of<IConsoleRedirectionMarker>(o => o.IsInputRedirected == isRedirected));
            Assert.Equal(isRedirected, sut.IsInputRedirected);
        }

        [Fact]
        public void FindFileTest()
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(currentDirectory, "file.json"), new MockFileData("some json content") },
                { Path.Combine(currentDirectory, "demo", "info.json"), new MockFileData("[null, 5]") },
                { Path.Combine(currentDirectory, "demo", "image.gif"), new MockFileData(new byte[] { 0x12, 0x34, 0x56, 0xd2 }) }
            });
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            var files = sut.FindFiles(currentDirectory, "json");
            Assert.Single(files);
            var relativeFilePath = files.First();
            Assert.Equal("file.json", relativeFilePath);
        }

        [Fact]
        public void FindNonExistingFileTest()
        {
            var currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(currentDirectory, "file.txt"), new MockFileData("some text content") },
                { Path.Combine(currentDirectory, "demo", "info.json"), new MockFileData("[null, 5]") },
                { Path.Combine(currentDirectory, "demo", "image.gif"), new MockFileData(new byte[] { 0x12, 0x34, 0x56, 0xd2 }) }
            });
            var sut = new IOHandler(fileSystem, Mock.Of<IConsoleRedirectionMarker>());
            var files = sut.FindFiles(currentDirectory, "json");
            Assert.Empty(files);
        }
    }
}