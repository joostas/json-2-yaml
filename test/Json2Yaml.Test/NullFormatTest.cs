using System;
using Json2Yaml.Yaml;
using Xunit;

namespace Json2Yaml.Test
{
    public class NullFormatTest
    {
        [Theory]
        [InlineData("null", "null")]
        [InlineData("", "empty")]
        [InlineData("empty", "empty")]
        public void ToStringTest(string given, string expected)
        {
            var sut = new NullFormat(given);
            Assert.Equal(expected, sut.ToString());
        }

        [Theory]
        [InlineData(null)]
        [InlineData("asdfasf")]
        [InlineData("NuLL")]
        public void UnsupportedValuesThrowsArgumentException(string wrongValue) =>
            Assert.Throws<ArgumentException>(() => new NullFormat(wrongValue));
    }
}