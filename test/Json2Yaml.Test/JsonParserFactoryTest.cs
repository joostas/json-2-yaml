using Json2Yaml.Json;
using Xunit;

namespace Json2Yaml.Test;

public class JsonParserFactoryTest
{

    [Fact]
    public void NewtonsoftJsonParserIsCreated()
    {
        var sut = new JsonParserFactory();
        var parser = sut.Create(new JsonParserSettings(new JsonFramework("newtonsoft")));
        Assert.IsType<NewtonsoftJsonParser>(parser);
    }

    [Fact]
    public void SystemTextJsonParserIsCreated()
    {
        var sut = new JsonParserFactory();
        var parser = sut.Create(new JsonParserSettings(new JsonFramework("system")));
        Assert.IsType<SystemTextJsonParser>(parser);
    }
}
