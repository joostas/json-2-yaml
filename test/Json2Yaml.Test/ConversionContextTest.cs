using Json2Yaml.Input;
using Json2Yaml.Interfaces;
using Json2Yaml.Json;
using Json2Yaml.Utils;
using Moq;
using src.Json2Yaml.Utils;
using Xunit;

namespace Json2Yaml.Test;

public class ConversionContextTest
{
    [Fact]
    public void CannotDoConversionOnEmptyContext()
    {
        var sut = new ConversionContext();
        Assert.False(sut.PerformConversion);
    }

    [Fact]
    public void AfterSettingErrorConversionCannotBeDone()
    {
        var inputSettings = new InputSettings(InputType.Console, "test.json", false, JsonFramework.Default);
        var outputSettings = new OutputSettings("test.yaml", Mock.Of<IYamlFormatSettings>());
        var sut = new ConversionContext(inputSettings, outputSettings);
        Assert.True(sut.PerformConversion);
        sut.SetError("some error");
        Assert.False(sut.PerformConversion);
    }

    [Fact]
    public void AfterSettingInfoConversionCannnotBeDone()
    {
        var inputSettings = new InputSettings(InputType.Console, "test.json", false, JsonFramework.Default);
        var outputSettings = new OutputSettings("test.yaml", Mock.Of<IYamlFormatSettings>());
        var sut = new ConversionContext(inputSettings, outputSettings);
        Assert.True(sut.PerformConversion);
        sut.SetInfo("some information");
        Assert.False(sut.PerformConversion);
    }
}
