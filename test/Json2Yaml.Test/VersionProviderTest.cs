using System;
using Json2Yaml.Interfaces;
using Json2Yaml.Utils;
using Moq;
using Xunit;

namespace Json2Yaml.Test
{
    public class VersionProviderTest
    {
        [Theory]
        [InlineData("0.2.0-alpha")]
        [InlineData("0.2.0")]
        public void SameVersionAsProvided(string version)
        {
            var sut = new VersionProvider(() => version, Mock.Of<IOutputWriter>());
            var result = sut.ReadProjectVersion();
            Assert.Equal(version, result);
        }

        [Fact]
        public void EmptyStringReturnsDash()
        {
            var sut = new VersionProvider(() => string.Empty, Mock.Of<IOutputWriter>());
            var result = sut.ReadProjectVersion();
            Assert.Equal("-", result);
        }

        [Fact]
        public void NullVersionReturnsDash()
        {
            var sut = new VersionProvider(() => null, Mock.Of<IOutputWriter>());
            var result = sut.ReadProjectVersion();
            Assert.Equal("-", result);
        }

        [Fact]
        public void IfGetVersionThrows_ReturnsDash()
        {
            var sut = new VersionProvider(() => throw null, Mock.Of<IOutputWriter>());
            var result = sut.ReadProjectVersion();
            Assert.Equal("-", result);
        }

        [Fact]
        public void NullVersionFuncThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new VersionProvider(null, Mock.Of<IOutputWriter>()));
        }

        [Fact]
        public void NullIOutputProviderThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new VersionProvider(Mock.Of<Func<string>>(), null));
        }
    }
}