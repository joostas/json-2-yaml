using System.Text;
using Json2Yaml.Cli;
using Xunit;

namespace Json2Yaml.Test
{
    public class OptionInfoTest
    {
        [Fact]
        public void HasValueForRefferenceType()
        {
            var sut = new OptionInfo<StringBuilder>(true, new StringBuilder());
            Assert.True(sut.HasValue);
        }

        [Fact]
        public void NoValueForRefferenceType()
        {
            var sut = new OptionInfo<StringBuilder>(true, null);
            Assert.False(sut.HasValue);
        }

        [Fact]
        public void HasValueForValueType()
        {
            var sut = new OptionInfo<int>(true, 8);
            Assert.True(sut.HasValue);
        }

        [Fact]
        public void NoValueForValueType()
        {
            var sut = new OptionInfo<int>(true, 0);
            Assert.False(sut.HasValue);
        }

        [Fact]
        public void HasValueForNullableValueType()
        {
            var sut = new OptionInfo<int?>(true, 8);
            Assert.True(sut.HasValue);
        }

        [Fact]
        public void NoValueForNullableValueType()
        {
            var sut = new OptionInfo<int?>(true, null);
            Assert.False(sut.HasValue);
        }
    }
}