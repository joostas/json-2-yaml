# Nuget packing

## Steps
* Update version information in csproj
> \<VersionPrefix>
* `dotnet pack -c Release [--version-suffix alpha|beta]` (creates nuget package)
* install the tool from newly created package:

    Before installing tool you need to uninstall previous one:

    `dotnet tool uninstall json2yaml -g`

    `dotnet tool install --global --add-source ./nupkg [--version version-alpha] json2yaml`

    --add-source adds local package store here

    --version is needed if this is prerelease (i.e alpha)
* publish to nuget.org

    `dotnet nuget push .\nupkg\json2yaml.*.nupkg -s https://api.nuget.org/v3/index.json -k <key>`

* go to nuget.org to update documentation.

## Links
[Example create dotnet tool (ms docs)](https://docs.microsoft.com/en-us/dotnet/core/tools/global-tools-how-to-create#setup-the-global-tool)

[Example publishing nuget package (ms docs)](https://docs.microsoft.com/en-us/nuget/create-packages/publish-a-package)

[Blog: How to publish a dotnet global tool with .NET Core 2.1](https://www.meziantou.net/2018/06/11/how-to-publish-a-dotnet-global-tool-with-net-core-2-1)

[nuspec (ms docs)](https://docs.microsoft.com/en-us/nuget/reference/nuspec)

[dotnet pack (ms docs)](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-pack?tabs=netcore2x)

[dotnet nuget push (ms docs)](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-nuget-push?tabs=netcore21)